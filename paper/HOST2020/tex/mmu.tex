\section{Targeting the \gls{MMU}}
\label{sec:mmu}

The \gls{MMU} is a critical component of \glspl{SoC}. It is in charge of the
virtual memory mechanism. In this section, the fault changes the virtual to
physical memory mapping albeit in an uncontrolled manner. The targeted
application is the same as in section~\ref{sec:icache}, shown on
\autoref{lst:loop}.

% \subsection{Experiments}

% Virtual memory is one of the critical technique use to ensure the security of the system.
% Usually, permissions are given individually to pages: read, write and execute, forbidding for exemple to execute data.
% Faults on the \gls{MMU} could therefore bypass important security features.

\subsection{The configuration of a working \gls{MMU}}

To understand the effect of the fault, we begin to explore the state of a
working application (without any fault). This state is a legitimate one.

\subsubsection{Page tables}
The page tables are used to memorize the mapping between virtual and physical
memory. In our configuration, we have 3 \glspl{PTD} (mapping
\SI{512}{\mega\byte} chunks) and %as show on \autoref{fig:l2pt}.
for each one, we have $8192$ \gls{PTE} pages of \SI{64}{\kilo\byte}. We show an
excerpt of the correct \glspl{PTE} on \autoref{fig:l3pt}.

% The \texttt{TTBR\_EL0} register holds the address to the start of the Level2 page table.

\begin{figure}
    \VerbatimInput[fontsize=\small,commandchars=\\\{\}]{Figures/memory_dumps/level3_page_tables_ok_half.dump}
    \caption{Memory dump excerpt for \glspl{PTE} before fault.\label{fig:l3pt}}
  \end{figure}

% \lstinputlisting[caption={Memory for level 2 page tables.},label=fig:l2pt]{Figures/memory_dumps/level2_page_tables_ok.dump}

% \lstinputlisting[caption={Memory for level 3 page tables.},label=fig:l3pt]{Figures/memory_dumps/level3_page_tables_ok_half.dump}


In the page tables, the most and least significant bits are used for the page
metadata (access rights, caches configuration, \textit{etc.}).

\subsubsection{\gls{TLB}}

\glspl{TLB} (plural since there are several of them) are small buffers used to
speed up virtual to physical memory translation. As in a cache memory, the last
mappings are saved to be reused later without a full page tables walk by the
\gls{MMU}. In the targeted \gls{SoC}, \gls{TLB} hierarchy mirrors cache
hierarchy: the \gls{TLB} designates the unified Level2 buffer while
micro-\glspl{TLB} are dedicated to instructions or data in each core.

\subsubsection{\Acrlong{OS}}

In our bare metal application, all the pages are initialized in the page tables
with an identity mapping (virtual and physical addresses are identical). In a
system with an \gls{OS}, pages are allocated on-the-fly. On the one hand, this
simplifies the forensic analysis since we are sure that page tables are correct
prior to the fault. On the other hand, interesting faults may be missed if the
\gls{OS} page allocation is disrupted.


\subsection{Forensic}

% There are two main methods used to examine memory mappings: the first one is to read
% memory with the \gls{JTAG} and to find where it should be by comparing memory
% dumps with and without a fault. The second method is the use of a pair of
% instructions computing the physical address (and the corresponding metadata) for
% a given virtual one.
% (cf \autoref{fig:vapa})

% \begin{lstlisting}[label=fig:vapa,float]
%     at	s1e3r, x0
%     mrs	x0, par_el1
% \end{lstlisting}

To reconstruct the memory mapping, we use a pair of instruction computing the physical address (and the corresponding metadata) for a given virtual one.
A script has been designed to extract the memory mapping. By using the \gls{JTAG}, first the two instructions \verb|at s1e3r, x0; mrs x0, PAREL1| are written at a given address, then the
\texttt{x0} register is set to one virtual address, the two instructions are
executed and finally the \texttt{x0} register contains the corresponding
physical address.

With this method, we compare the memory mappings with (\autoref{fig:faulted_mapping}) and without (\autoref{fig:identity_mapping}) a fault.
 
\begin{figure}
    \centering
    \BVerbatimInput[fontsize=\small]{Figures/memory_mapping/identity.txt}
    \caption{Correct identity mapping}
    \label{fig:identity_mapping}
\end{figure}

\begin{figure}
  \centering
  \BVerbatimInput[fontsize=\small]{Figures/memory_mapping/working_fault.txt}
  \caption{Mapping after fault}
  \label{fig:faulted_mapping}
\end{figure}

% \vspace{-0.5cm}

Three different effects can be observed depending on the page:

\begin{itemize}
\item Pages are correct with an identity mapping up to \texttt{0x70000}.
  Remarkably theses are all the pages used to map our application in memory.
  Therefore, a hypothesis is that the corresponding translations are present in
  caches and are not impacted by the fault.
\item Pages are incorrectly mapped to \texttt{0x0}. A read at \texttt{0x80000}
  reads with success physical memory at \texttt{0x0}.
\item Pages are shifted. A read at \texttt{0xc0000} reads physical memory at
  \texttt{0x80000}.
\end{itemize}

If we invalidate the \gls{TLB} after a fault, nothing changes: the mapping stays
modified.
We conclude that the fault does not affect the cache mechanism of address translation  (at least what can be invalidated by software) but directly the \gls{MMU}.

To look for an explanation of the incorrect mapping,
we can look for the impact on page tables on \autoref{fig:l3xxxxpt}.


% \lstinputlisting[caption={Memory for level 2 page tables after a fault.},label=fig:l2xxxpt]{Figures/memory_dumps/level2_page_tables_fault.dump}

% \lstinputlisting[caption={Memory for level 3 page tables after a fault.},label=fig:l3xxxxpt]{Figures/memory_dumps/level3_page_tables_fault_half2.dump}

\begin{figure}
  \VerbatimInput[fontsize=\small,commandchars=\\\{\}]{Figures/memory_dumps/level3_page_tables_fault_half2.dump}
  \caption{Memory dump excerpt for \glspl{PTE} after a fault.\label{fig:l3xxxxpt}}
\end{figure}

% \TODO{pourquoi on regarde les pages tables ? pour expliquer la faute}
The fault on the \gls{MMU} has shifted the page tables in memory, and has
inserted errors in it. Since the memory translation is still valid after the
fault, and do not correspond to the shifted page tables, this shift is not the only source of incorrect translation.
Either the page walk is done from physical addresses and/or some \gls{TLB} are
not properly invalidated when we try to.

%\TODO{montrer les limites de l'analyse}
% Unfortunately we did not find any way to properly validate or invalidate these hypotheses.

\subsection{Exploit}

This fault shows that the cornerstone of the key security feature in any
\gls{SoC}, namely memory isolation, does not withstand fault injection. In
\cite{drammer}, the authors use the rowhammer attack to fault a \gls{PTE}. The
faulted \gls{PTE} accesses the kernel memory which allow the attacker to obtain a privilege escalation: by overwriting an userland \gls{PTE} for accessing all the
memory,  by changing the user ID to root or by changing the entry point of an executable.

% But two big unknowns remain.
% \begin{itemize}
% \item We lack control of the final faulty mapping. Can we influence it? With the
%   same injection parameters the same mappings are reproducible. And with
%   different parameters, different mappings were obtained. But can we control the
%   mappings?
% \item To properly answer the previous question, we cannot forget the role of the
%   \gls{OS}. How would it react to an improper mapping? What would be the
%   consequences?
% \end{itemize}

Additionally, this fault model is a threat to pointer authentication
countermeasures, as proposed in the recent ARMv8.3 ISA. This pointer protection
works by storing authentication metadata in the most significant bits (usually
useless) of a pointer value. To use a pointer, the chip first validates the
authentication metadata. In our case, the attacker does not need to alter the
pointer value, it can alter where it physically points to, at a coarse (page)
granularity.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
