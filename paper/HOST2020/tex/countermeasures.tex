\section{Countermeasures}
\label{sec:countermeasures}

% \cite{DangerFGHKMT18}

The vulnerabilities described above can be summarized as integrity and authenticity problems: instructions
(or data) are altered between their storage in memory and their reuse. 
If we consider that only a legitimate entity can write to memory, then integrity is the only problem.
In the other case, we must also ensure the write authenticity.

\subsection{Ensuring integrity}

Ensuring the integrity of signals in a chip is a well known problem that had to be solved to allow the use of chips in harsh environments: space or nuclear reactors for example.
Yet one must account for a different threat model: when designing for security, the fault value must be considered intentional, not drawn from a uniformly random distribution.

To ensure integrity, designers use redundancy. We can duplicate the core, executing exactly the same things on both cores and verifying that results are identical. Redundancy can also be achieved using error detection codes. Of course, redundancy has an overhead but this is the cost of guaranteeing the integrity.

When dealing with the threat of physical attacks, ensuring integrity is often not enough. If we consider that the attacker can modify memory, she could bypass the error detection code or write the same error value in the duplicated memories.
In this case, we must ensure the data authenticity.
With cryptography, authenticity can be guaranteed by relying on \glspl{MAC}.
We can imagine a strategy based on this principle to be able, in hardware, to detect changes in data or instructions.


\subsection{\gls{MAC} Generation}

A solution, to ensure authenticity has been proposed with SOFIA~\cite{SOFIA}.
For each data or instruction block (the block size has to be adapted to the
micro-architecture), the objective is to calculate and associate a \gls{MAC} to
detect any alteration. In addition to the data itself, the \gls{MAC} calculation
must also be address dependent to detect shifts between an address and the
corresponding data (as observed in the L2 cache in section \ref{sec:l2cache}).
\glspl{MAC} must be generated at the right time: in the case of instructions,
they can be computed at compile time. But for the data, it must be possible to
do the generation at the pipeline output (during memory access).

\subsection{\gls{MAC} Verification}
Depending on the energy consumption/performance trade-off, two implementation strategies can then be considered on a
system-wide basis to perform the \gls{MAC} check.

\begin{itemize}
\item \textbf{Just-in-time:} To minimize the overall activity, this
  strategy consists in bringing the data and its \gls{MAC} back to the \gls{CPU}
  (or \gls{MMU}) in a classical way. The verification would then be performed by
  the \gls{CPU} just before data consumption. In case of mismatch, a request has
  to be made to the higher memory level (the L1 cache) to perform a
  verification on its own data version. If the new verification is
  successful, the data would be transmitted to the \gls{CPU} and if not, a
  request to the next level (L2) has to be made. Thus, several checks are
  performed only when necessary, but the cost of a mismatch (that can be due to
  environmental radiations) is very high.
\item \textbf{Proactive:} A second strategy can be used to reduce the time
  penalty in case of error. Integrity checks are automatically performed at each
  level of the memory hierarchy. Thus, errors can be detected early, before they
  reach the \gls{CPU}. Each cache level can ensure independently that it has
  only valid data, but the energy consumption would be higher.
\end{itemize}



%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
