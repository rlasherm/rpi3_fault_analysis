\subsection{Related work}
\label{sec:previous}

Since the seminal works of Boneh \textit{et al.}~\cite{boneh_faults} and Biham
and Shamir~\cite{biham_faults}, we know that fault injection attacks are a threat to the
security of cryptographic implementations. Research on this topic has split in two
directions: on the one hand, the theoretical axis is interested in how a fault
can weaken the security of an application, mainly cryptographic algorithms
(\textit{e.g.}~\cite{piret_fa} and~\cite{giraud_fa} on AES).

On the other hand, the question is what faults can practically be performed in
systems. A formal description of these achievable faults is called a fault model
and it is this description that is used for theoretical analysis.

A fault model is always the interpretation of a physical behaviour at a
specific abstraction level. In other words, the same fault can be formalized
differently if we look at the transistor, the \gls{ISA} or the
software levels. Therefore, different studies looked at different levels for the
faults effects.

Several groups have developed fault models for
microcontrollers. Balasch \textit{et al.}~\cite{BalaschGV11} performed a clock
glitch attack on an 8-bit AVR microcontroller embedded in a smartcard. They show
that one can replace instructions in the execution flow by either targeting the
fetch or the execute stages of the 2-stage pipeline. 

ARMv7-M microcontrollers have been thoroughly studied, in particular the Cortex-M3
\cite{MoroDHRE13} and Cortex-M4 \cite{RiviereNRDBS15}. These papers highlight
that \gls{EMFI} on these devices disturbs the correct behaviour of the pipeline.
Moro \textit{et al.}~\cite{MoroDHRE13} show that a fault can modify the fetched
opcode (at the time of fault injection) and that the new opcode has no side
effects with high probability. It is therefore equivalent to a \texttt{NOP} instruction
(no-operation). Another observed effect is the modification of the data in a
\texttt{LOAD} instruction. According to Rivière \textit{et al.}~\cite{RiviereNRDBS15},
the fault suppresses the instructions fetch\footnote{up to 4 instructions may be fetched
in one clock cycle}. The previously loaded instructions are instead executed
again before resuming correct execution, and the disrupted instructions are
never executed. These works only focus on microcontrollers.

But they are simple systems, they have in-order pipelines, most of
the time only one core, a simple memory hierarchy (only L1
cache if any) and no support for virtual memory. More recent works have been
invested in trying to fault more complex processors, mainly ARM \glspl{SoC}.

Timmers \textit{et al.}~\cite{TimmersSW16} show how to attack an ARMv7-A chip by
taking control of the \gls{PC} with fault injection attacks. Cui and Housley~\cite{BADFET}
demonstrate an \gls{EMFI} targeting the communication between the \gls{CPU} and
the external memory chips. The authors insist particularly on the difficulty to
achieve the required temporal and spatial resolution for \gls{EMFI} on modern
\gls{SoC}. Majéric \textit{et al.}~\cite{MajericBB2016} discuss how to find the
correct \gls{EMFI} parameters and set up a fault injection on the AES
coprocessor to a Cortex-A9 core.

A critical feature of modern \glspl{SoC} is their complex micro-architecture,
increasing the attack surface. As a consequence, it is now possible to obtain
hardware faults from software execution. Tang~\textit{et al.}~\cite{TangSS17}
achieve a fault by taking control of the microcontroller in charges of
monitoring and managing the energy of the \gls{SoC}. By software means, they are
able to modify the power voltage and the clock frequency. DRAM memories are also
susceptible to Rowhammer attacks~\cite{VeenFLGMVBRG16}: with specific access
patterns, one can switch bits into the memory chip. The gap between the
\gls{ISA} abstraction and its real implementation (notably in Out-of-Order
processors) leads to the Meltdown~\cite{MELTDOWN} and Spectre~\cite{SPECTRE}
vulnerabilities.

Today, we understand the risks due to the complex micro-architecture of modern
\glspl{SoC}. Fault injection attacks have been demonstrated to be a threat to these
systems. Proy \textit{et al.}~\cite{proy2019} have proposed a fault model characterization on \gls{SoC} (with an \gls{OS}) at the \gls{ISA} level. But the fault effects on the \gls{SoC}
micro-architecture have still not been evaluated.

Our work intends to bring this missing piece to our understanding of
the security model of modern \glspl{SoC}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
