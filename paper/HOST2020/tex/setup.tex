\section{Fault injection on embedded systems}
\label{sec:fault_injection}
\subsection{The targeted chip: The BCM2837 on the Raspberry Pi 3 B}
\label{sec:setup}

\subsubsection{Presentation}

The \acrfull{RPi3} is a low cost single board computer. It features a
complete system able to run a complex OS such as Linux or Windows and their
applications. The \gls{SoC} powering this board is the BCM2837 from Broadcom, a
quad-core Cortex-A53 \gls{CPU} running at \SI{1.2}{\giga\hertz} with the help of
a dual core VideoCore IV GPU at \SI{400}{\mega\hertz}.

Our experiments are performed with our own software stack\footnote{Released as
  open-source software (MIT Licence). The git repository is available at
  \textit{blinded for reviews}}, with only one core active. To control the
behaviour of the chip, we have implemented the bare minimum to run our
applications: initialization of \gls{JTAG}, UART, GPIO, \gls{CPU} caches and
\gls{MMU}. We want to stress out that no \gls{OS} is running during our
experiment in sections \ref{sec:icache}, \ref{sec:mmu} and \ref{sec:l2cache}, to
avoid interference that could hinder our ability to infer the fault models. In
particular, we want to avoid the effects of context switching due to preemptive
scheduling by the \gls{OS}, the error recovery mechanisms (if an error occurs,
we want to know) and the caches maintenance performed by the \gls{OS}.

In order to later explain the causes of the failures observed, we describe
in more details two important subsystems of this \gls{SoC}: the cache hierarchy
and the \gls{MMU}.


\subsubsection{Cache hierarchy}

In modern systems, memory accesses are a lot slower than the \gls{ALU}. To avoid
loosing too much performance to this latency difference, small and fast memories
called caches are used to mirror a part of the memory space.

In the targeted \gls{SoC}, each core has two L1 caches (the smallest and fastest
kind), one dedicated to instructions (L1I), one dedicated to data (L1D). These
caches are \SI{16}{\kilo\byte} with \SI{64}{\byte} line width.

Then a second layer of cache, the L2 cache, is common to all cores and thus
provides a unified view of the memory space. Its size is \SI{512}{\kilo\byte}
with \SI{64}{\byte} line width.

\begin{figure}[h]
  \centering
    \includegraphics[width=0.3\textwidth]{Figures/BCM2837_caches.png}
    \caption{Memory hierarchy for the BCM2837.}
    \label{fig:memhier}
\end{figure}

% \begin{figure}
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{Figures/BCM2837_caches.png}
% 	\caption{Cache memory hierarchy\label{fig:caches}}
% \end{figure}

% \paragraph{\gls{MMU}}

% \TODO{Peut être redondant avec la section rajoutée par Thomas}

% Our system bases its memory isolation solution on the virtual memory
% abstraction. Any process works as if the whole memory space is available to it.
% Behind the scene, the process uses virtual addresses that are mapped, on the
% fly, to physicall addresses. The mechanism used to ensure that this mapping is
% done correctly and with a low latency is a mix of software and hardware system.

% On the software side, the \gls{OS} (when there is one), is responsible for
% specifying the mapping: \textit{i.e.} which physical address is mapped from some
% virtual one. This mapping is stored in the page tables, a memory section that
% specify for each page (here a 64kB virtual memory chunk) the correponding
% physical address as well as access rights assotiated to it. In a standard
% \gls{OS}/applications setting, each process has its dedicated page tables which
% ensure a strong form of memory isolation between processes. In our simple
% implementation, we have a single application. The \gls{MMU} is enabled but with
% an identity mapping: physical addresses and virtual ones are identical. One has
% to note that the \gls{MMU} has to be enable to run a program: instructions
% alignment requirements are not the same with or without the \gls{MMU} enabled and
% our compiler (Linaro GCC 6.2-2016.11) requires the \gls{MMU}.

\subsubsection{\gls{MMU}}

The \gls{MMU} is a central component for every multi-applications system. It
aims at virtualizing the physical memory of the system into a virtual one.
Therefore, the CPU only works with virtual addresses and during a memory access
to one of these addresses, the \gls{MMU} translates it into the corresponding
physical address which is transmitted to the memory controller of the system.
The information required for the translation of an address is called a \gls{PTE}
and it is stored in the physical memory and cached in the \gls{TLB}. There is a
\gls{PTE} for every allocated pages in the physical memory. Our bare metal
implementation allocates the whole address space with an identity mapping
(virtual and physical addresses are the same) with \SI{64}{\kilo\byte} pages.

In modern systems, the translation phase does not only compute the physical
address but also realizes different checks. These checks are monitoring if the
page can be written or not, which kind of process (user or supervisor) can
access it or should the page be stored in cache or not.

Among all its roles, the \gls{MMU} is also a security mechanism. Ensuring that
a read-only page cannot be written to and ensuring that only authorized processes
can access their corresponding pages. This last security mechanism is the memory
partitioning. On multi-applications systems, it avoids a process to spy or
corrupt the memory used by another process.

On complex \glspl{OS}, the \gls{MMU} and the \gls{PTE} are setup by the
kernel and are critical assets.


% \paragraph{Memory coherence}

\subsection{The Electromagnetic fault injection bench}%: Faustine}

To inject faults on the BCM2837, some apparatus is required. Our experimental
setup has been designed to be highly configurable and to work at higher
frequencies than most setups targeting microcontrollers. First, we use a
Keysight 33509B to control the delay between a trigger issued by the \gls{RPi3} board
before the instructions of interest. The Keysight 81160A generates the signal
for the EM pulse: one sinus period at \SI{275}{\mega\hertz} with a power of
\SI{-14}{\dBm}. A sinus is used instead of the usual pulse since it gives fewer
harmonics at the output of the signal generation chain. Then, this signal is
amplified with a Milmega 80RF1000-175 (\SI{80}{\mega\hertz} -
\SI{1}{\giga\hertz} bandwidth). Finally, the high power signal is connected to a
Langer RF U 5-2 near field probe. A part of this energy is therefore transmitted
into the metallic lines of the chip, which can lead to a fault.

The minimum latency between the initial trigger and the faulting signal reaching
the target is high: around \SI{700}{\nano\second}. As a consequence, the
targeted application must be long enough to be reachable by our fault injection
bench.

\subsection{Synchronization}

% Latency and jitter
The main difficulty for fault injection is the synchronization: how to inject
precisely a fault on the targeted and vulnerable instructions.

To resolve this point, we need a temporal reference, given here by a GPIO: an
electrical rising edge is sent to a board pin by our application just before the
area of interest. Our setup is using the evaluator approach: the attacker can instrument the system to ease the experiments. In the case of a
real attack, the adversary would have to generate this trigger signal: it can be
done by monitoring communications, IOs, or EM radiation to detect patterns of
interest. In all cases, it is a tricky business highly application dependant.

But the trigger signal is just part of the problem: from this instant we must
wait the correct moment to inject the fault. To give a sense of the experimental
difficulty: for a chip running at \SI{1}{\giga\hertz}, a clock period lasts
\SI{1}{\nano\second}. In this lapse of time, light travels only for around
\SI{30}{\centi\meter}. Propagation times are not negligible. On a modern
\gls{SoC}, the matter is made more difficult by the memory hierarchy. Since
cache misses are highly unpredictable, they imply a corresponding jitter. It is
hard to precisely predict the duration of a memory access and therefore the time
to wait to inject the fault.

Synchronization is a problem, but not a hurdle that much. Indeed, the
attacker has only to inject faults until the correct effect is achieved. Because
of the jitter, for the same delay (time waited between fault injection and
trigger), different timing will be tested with respect to the running program.
If a fault with an interesting effect is possible, it will eventually be
achieved.

Additionally, as we will see in the next sections, memory transfers are
particularly vulnerable to \gls{EMFI}. They are also slower than the core
pipeline, allowing for a bigger fault injection timing window.

\subsection{How to change the fault effect?}

Fault effects are reproducible with a low ratio; meaning that if a fault has
been achieved, it will be achieved again with the same parameters but only for a
small ratio of the fault injections. In the other cases, no failure occurs or
another effect is observed (mostly due to jitter). To modify the fault effect,
the main parameters are the timing and the position of the probe over the
component. In particular, the signal parameters (shape, frequency, number of
periods) have an optimal value with respect to our requirements. The frequency
and the shape are chosen to maximize the EM coupling, the number of periods is
fixed to have the best timing precision.
% One can also modify the angular position of the
% probe to modify the coupling direction. But this parameter has not been explored
% since our experimental bench lacks the apparatus required to automatically and
% precisely modify it.

\subsection{Forensic methodology}

As the targeted system is a closed box, we have a limited mean to
explore what is happening in the system, namely the \gls{JTAG}. With it, we are
able to halt the chip execution to read the register values and to read memory
as seen by a particular core (with a data viewpoint). Therefore, to pinpoint the
particular effects of a fault injection, we trade observability of the system
with controllability: we force the system state such that an observable change
gives us information on the fault mechanism. To maintain controllability, our
software footprint has to be minimal. As such we will not describe how to breach a
particular system with our faults since any exploit is highly application
dependant and our setup is not representative of a standard application
environment. Instead we will suggest exploit strategies: how such faults could
be used by a malicious attacker?

% \subsection{Targeted application}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
