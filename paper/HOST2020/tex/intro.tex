\section{Introduction}

From a world where each application has its dedicated hardware support (high
performance chips for desktop computers, low-power chips for mobile phones,
hardened chips for smartcards, \textit{etc.}), more and more applications
are today executed on the same device: the smartphone. They are powered by fast and
low-power components, the \glspl{SoC}, where a set of modules (modem, graphic
and sound card, Flash memory, \textit{etc.}) takes place on the same silicon
layout as the \gls{CPU}. With the democratization of smartphones, more sensitive
processes are done on the same component as the usual non sensitive processing. Aware of
this risk, vendors invest a lot of work into the software layer hardening
with numerous security mechanisms embedded into the \gls{OS} (such as Android or
iOS). However, the hardware layer misses the same attention: in particular it is
not protected against physical attacks, especially against fault injection
attacks.

Fault injection attacks is a well-known class of attacks where an attacker modifies the
physical environment of the targeted chip in order to induce a fault during its
execution. The resulting failure can be used to extract sensitive information
(cryptographic keys) or bypass a PIN code verification for instance. More
generally, fault injection attacks give the ability to modify a program at runtime,
defeating static countermeasures that cannot foresee the failure (\textit{e.g.} secure
boot, access control).

The attacker's ability to subdue the system is highly dependant of its
experimental capacities: fault injection attacks can be performed with power glitches,
clock glitches, lasers, electromagnetic pulses, \textit{etc.} In this paper, we
propose an \gls{EMFI} attack against a \gls{SoC}. Electromagnetic pulses modify
the electric signals in the metallic chip wires. Faults are generated
when signals are modified during a small time window around-the-clock rising
edge. In this case, according to Ordas \textit{et al.}~\cite{OrdasGM15}, a
faulty value may be memorized.

Most previous hardware attacks target microcontrollers. Indeed, these chips are
slower and simpler than \glspl{SoC}. Therefore, an attacker can easily perturb
microcontrollers and exploit a fault.

\subsection{Motivation}
\label{sec:motivation}

The \gls{SoC} security model is focused almost entirely on software, so much
that hardware countermeasures such as TrustZone explicitly exclude hardware
security from their objectives. Fault injection attacks on \glspl{SoC} is a recent
research topic where some published articles
\cite{TimmersSW16,BADFET,MajericBB2016} focused on breaking software security
properties.

Fault injection is a versatile tool allowing to modify a program behaviour at
runtime, to inject vulnerabilities in a sound (and even proved)
software. The stake is the system security as a whole, as in controlling
what software is executed and what data can be accessed by whom. With software
security constantly improving and the costs and experimental difficulty of
performing fault injection attacks declining, we can surmise that the latter will become
a major threat in the future.

Understanding the \gls{EMFI} effects on a \gls{SoC} is required to understand
the threat and to design effective countermeasures. There is an extensive
literature on fault injection attacks on microcontrollers, and as a result, the most
secure devices against them are derived from microcontrollers (aka secure
components). The same work has to be done for \glspl{SoC}.

\input{tex/previous}

\subsection{Contributions}
\label{sec:contribution}

In this article, we focus on an ARMv8 \gls{SoC}, namely the Broadcom BCM2837 chip
at the heart of the \gls{RPi3}. It is a widely successful low cost single
board computer. This quad-core Cortex-A53 \gls{CPU} runs at
\SI{1.2}{\giga\hertz} and features a modern ``smartphone class'' processor. We
are using \gls{EMFI} and observe the resulting failures to deduce their origins. 

We observe radically new fault models that are neither described in other works
nor taken into account when discussing the modern embedded systems security.
In this paper, we demonstrate how we recovered these fault models and provide insights on the micro-architectural mechanisms leading to these models. 

The consequences are dire: a \gls{SoC} must not be considered as a black box
with respect to security. It is not enough to work on the software side security
if it does not rely on solid hardware foundations.\newline

The goal is to provide a micro-architectural explanation of the observed
behaviour. To that end, we have to control the targeted system and limit its
complexity. It implies most notably that we use a single-core configuration and
setup an identity mapping for virtual memory. This simplification choice does
not imply necessarily a harder exploitation on more realistic systems, since the discovered fault models would still be present.
But on such systems, it becomes hard to isolate the effect of a fault and attribute it to one subsystem: we cannot propose a simple model explaining the observed behaviour.

We describe our setup in
\autoref{sec:setup}, both the experimental apparatus used to inject faults but
also the targeted hardware and software environment.

To stress out the software layer impact on the observed failures, we
compare the faults observability with and without an \gls{OS} in section~\ref{sec:os}.

Observed faults on a bare-metal setup are analysed in sections \ref{sec:icache},
\ref{sec:mmu} and \ref{sec:l2cache}. For each fault category, we will explain
the process that allows us to infer the cause of the failure. The possibility to
exploit these faults will be discussed as well as the experimental difficulties
to achieve them. We finish with propositions to protect \glspl{SoC} against
these attacks in \autoref{sec:countermeasures} and conclude in
\autoref{sec:conclusion}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
