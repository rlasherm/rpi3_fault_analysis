\newdate{date_raspbian}{18}{04}{2018}

\section{Impact of the \gls{OS}}
\label{sec:os}

To support our choice of bare metal applications to understand the fault model,
in this section we compare the faults observed with or without an \gls{OS} for
the same \gls{EMFI} parameters.

\subsection{Sensibility maps for the BCM2837}
Knowing where to place the probe for obtaining interesting effects is mandatory
for every perturbation experiment. Therefore, the first step consists in doing a
sensibility map of the component against EM perturbations.

During our experiments, two different setups were tested. The first one was
running the target program \autoref{lst:loop} on a bare metal system (one core
with only UART and \gls{JTAG} enabled). The second one was running the same
program as an application on a Linux-based OS\footnote{Raspbian Lite released on
  \displaydate{date_raspbian} available here:
  \url{https://downloads.raspberrypi.org/raspbian_lite/archive/2018-06-29-03:25/}}.

\lstinputlisting[language=Python,caption={Loop target
  application},label=lst:loop]{Figures/programs/loop.c}

The \autoref{fig:bcm2837_carto} shows the two sensibility maps with the number
of effects induced by the perturbations for every probe location over the SoC.
The first conclusion is that the sensibility of the component under EM
perturbations depends on what is running on it. The setup running with Linux has
a wider sensitive area than the bare metal one. However the sensitive area of
the bare metal setup is included in the Linux one. This suggests that the two
setups behave similarly under the perturbations on this area. Since the Linux
system embeds a far more complex piece of software than the bare metal one, with
more enabled interfaces, it may explain that the Linux setup has a wider
sensitive area.

\begin{figure}[h]
  \begin{minipage}[t]{0.5\linewidth}
    \includegraphics[width=\textwidth]{Figures/raspi3_carto/reboot_carto_4040_baremetal}
    \subcaption{Bare metal}
    \label{fig:rpi3_carto_bare_metal}
  \end{minipage}
  \begin{minipage}[t]{0.5\linewidth}
    \includegraphics[width=\textwidth]{Figures/raspi3_carto/reboot_carto_4040_linux}
    \subcaption{Linux}
    \label{fig:rpi3_carto_linux}
  \end{minipage}
  \caption{Sensibility of the BCM2837 against EM perturbations: number of effects for every position over the \gls{SoC}.}
  \label{fig:bcm2837_carto}
\end{figure}

\subsection{Faults on bare metal versus faults on Linux}

The sensitive areas are not the only differences of behaviour between the two setups.
Another difference is the impacts of the perturbations.
In other words, the faults obtained are different on the bare metal setup and the Linux setup.
More precisely, the observable effect of a fault as seen by the same application is different whether it runs on a bare metal setup or on a Linux setup.

Evaluate the fault model on the Linux setup is a complex analysis, with a different approach than applied in this paper and therefore requiring its own dedicated paper.
To summarize, with an \gls{OS} the faults are observable at the instruction level.
This means the effect induced by the perturbation is equivalent to modify one or several instructions of the executed program.

On the bare metal setup, there are no observable faults at the instruction level but instead at the micro-architectural level, as shown in the next sections.
The effect of the fault is equivalent to modifying the behavior (signals or configuration for instance) of subsystems like buses, MMU, memory, caches, etc.

This difference suggests that the usage of an \gls{OS} leads to a specific perturbation behavior.
In this specific case, the fault model induced by the \gls{OS} (instruction
modification) is easier to exploit than the fault model on bare-metal setup: the
\gls{OS} weakens the security of the system against fault attacks.\newline

In this work, we will focus on the fault model on the bare metal setup.
We will show how to analyze and reconstruct the effect of a perturbation on the micro-architectural elements of a processor.
 
However, we suppose that the micro-architectural effect of the fault on the bare metal setup explain the observed effect at the instruction level on the Linux setup.
% The \autoref{fig:abstraction_layers} summarizes the different layers of a digital system and the observability of the fault for the different setups.

% \begin{figure}[h]
%   \centering
%   \begin{adjustbox}{max totalsize={\linewidth}{\textheight},center}
%     \begin{tikzpicture}
%       \draw [fill=black!10, black!10] (0,0) rectangle (4,3);
%       \foreach \y/\name in {
%         0/Physical level,
%         1/Logical level,
%         2/Micro-architectural level,
%         3/Instruction level,
%         4/Application level,}
%       {
%         \draw (0,\y) rectangle (4,\y+1);
%         \node at(2,\y+0.5) {\name};
%       }
%       \node at(5,0.5) [right, align=left] {Effect of the\\perturbation};
%       \draw [->,>=latex,line width=0.05cm] (5,0.5) -- (4,0.5);

%       \node at(5,2.5) [right, align=left] {Observability on\\bare metal setup};
%       \draw [->,>=latex,line width=0.05cm] (5,2.5) -- (4,2.5);

%       \node at(-1,3.5) [left, align=right] {Observability on\\Linux setup};
%       \draw [->,>=latex,line width=0.05cm] (-1,3.5) -- (0,3.5);
%     \end{tikzpicture}
%   \end{adjustbox}
%   \caption{Abstraction layers \cite{Yuce2018}.}
%   \label{fig:abstraction_layers}
% \end{figure}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
