\section{Introduction}

From a world where each application has its dedicated hardware support (high
performance chips for desktop computers, low power chips for features phones,
hardened chips for smartcards, \ldots), today more and more applications are
executed on a same device: the smartphone. They are powered by fast and
low-power components, the \glspl{SoC}, where a set of modules (modem, graphic
and sound card, Flash, \dots) takes place on the same silicon layout as the \gls{CPU}.
With the democratization of smartphones, more sensitive processes are done on
the same component as the unsecure operations. Aware of this risk, vendors
invest a lot of work into the hardening of the software layer with numerous
security mechanisms embedded into the \gls{OS} (such as Android or iOS).
However, the hardware layer misses the same attention: in particular it is
not protected against physical attacks.

Fault attacks is a well known class of attacks where an attacker modifies the
physical environment of the targeted chip in order to induce a fault during its
execution. The resulting failure can be used to extract information
(cryptographic keys) or bypass a PIN code verification for instance. More
generally, fault attacks give the ability to modify a program at runtime,
defeating static countermeasures that cannot foresee the failure (e.g. Secure
boot, access control, \ldots).

The attacker's ability to subdue the system is highly dependant of its
experimental capacities: fault attacks can be performed with power glitches,
clock glitches, lasers, electromagnetic pulses, \ldots{} 
In this paper, we propose an \gls{EMFI} attack against a \gls{SoC}.
Electromagnetic pulses modify the electric signals in the metallic wires of the chip.
Faults are generated when signals are modified during a small time window around the clock rising edge.
In this case, according to Ordas \textit{et al.}~\cite{OrdasGM15}, a faulty value may be memorized.


Most previous hardware attacks target microcontrollers. Indeed, these chips are slower and
simpler than \glspl{SoC}. Therefore, the attacker can easily perturb these
components and exploit a fault.


\input{tex/previous}

% The consequence of a fault is also more predictable: the chip architecture being
% simple, the resulting fault models are relatively simple. Most secure chips,
% embeded in smartcards or secure elements, are in this category. Nowadays,
% sensitive operations are moving from secure chips to \gls{SoC}.
% \newline

\subsection{Motivation}
\label{sec:motivation}

Despite the fact that more and more sensitive services are implemented in
\glspl{SoC}. Fault attacks on them is an on-going work where some recently
published articles \cite{BADFET,MajericBB2016,TimmersSW16} focused on breaking
the software layer.

% But, enabling software attacks from physical means is a way
% to obtain sensitive assets (\textit{e.g.} contents protected by DRM or private
% user's data).

These attacks are trickier and more difficult to reproduce compared to software
attacks. For this reason, \glspl{SoC} manufacturers provide hardware-based
mechanisms to improve software security but do not implement hardware
countermeasures against physical attacks.\newline

In this work, we focus on how those hardware mechanisms can be corrupted to
break the software security. The hardware is also en entry
point for an attacker.

\subsection{Contributions}
\label{sec:contribution}

In this article, we focus on an ARMv8 \gls{SoC}, namely the Broadcom BCM2837 chip
at the heart of the Raspberry Pi 3 B. It is a widely successful low cost single
board computer. This quad Cortex-A53 cores \gls{CPU} runs at
\SI{1.2}{\giga\hertz} and features a modern ``smartphone class'' processor. We
are using \gls{EMFI} and observing the resulting failures to deduce their origins.

We observe radically new fault models that are neither described in other works
nor taken into account when discussing the security of modern embedded systems.
The consequences are dire: a \gls{SoC} must not be considered as a black box
with respect to security. It is not enough to work on the security of the
software side if it does not rely on solid hardware foundations.\newline

We describe our setup in
\autoref{sec:setup}, both the experimental apparatus used to inject faults but
also the targeted hardware and software environment.

To stress out the impact of the software layer on the observed failures, we
compare the observability of the faults with and without an \gls{OS} in section \ref{sec:os}.

Observed faults are analyzed in sections \ref{sec:icache}, \ref{sec:mmu} and
\ref{sec:l2cache}. For each fault category, we will explain the process that allows
us to infer the cause of the failure. The possibility to exploit these faults
will be discussed as well as the experimental difficulties to achieve them. We
finish with propositions to protect \glspl{SoC} against these attacks in
\autoref{sec:countermeasures} and conclude in \autoref{sec:conclusion}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-engine: luatex
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
