(TeX-add-style-hook
 "main"
 (lambda ()
   (setq TeX-command-extra-options
         "-shell-escape")
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("babel" "english") ("siunitx" "binary-units")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "Verbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "BVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "LVerbatim*")
   (add-to-list 'LaTeX-verbatim-environments-local "SaveVerbatim")
   (add-to-list 'LaTeX-verbatim-environments-local "VerbatimOut")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "Verb*")
   (TeX-run-style-hooks
    "latex2e"
    "tex/acronyms"
    "tex/abstract"
    "tex/intro"
    "tex/setup"
    "tex/os"
    "tex/l1icache"
    "tex/mmu"
    "tex/single_core_l2cache"
    "tex/countermeasures"
    "tex/conclusion"
    "article"
    "art10"
    "inputenc"
    "babel"
    "amsmath"
    "color"
    "amsfonts"
    "amssymb"
    "graphicx"
    "listings"
    "hyperref"
    "adjustbox"
    "glossaries"
    "fancyvrb"
    "siunitx"
    "subcaption"
    "eurosym"
    "tikz"
    "pgfplots"
    "datetime"
    "chngcntr")
   (TeX-add-symbols
    '("verbatimfont" 1)
    '("TODO" 1))
   (LaTeX-add-bibliographies
    "biblio")
   (LaTeX-add-color-definecolors
    "darkgreen")
   (LaTeX-add-siunitx-units
    "belmilliwatt"
    "dBm"))
 :latex)

