HOST 2020 Paper #3 Reviews and Comments
===========================================================================
Paper #3 Electromagnetic fault injection against a System-on-Chip, toward
new micro-architectural fault models


Review #3A
===========================================================================

Overall merit
-------------
4. Borderline

Reviewer expertise
------------------
4. Expert

Overview
--------
This submission proposes a method to create exploitable faults on an SoC.

Positive aspects of the paper
-----------------------------
+ Study electromagnetic attacks to SoC.
+ Analysis of a real target.
+ Highlights the presence of a larger attack surface when OS is running.

Negative aspects of the paper
-----------------------------
- Some of the conclusions are not entirely surprising/ new.
- The literature review is incomplete.
- Proposed mitigations are not evaluated or well described.

Recommendation
--------------
This submission proposes a method to create exploitable faults on an SoC.

One of the main contributions of this submission is on the experimentation of electromagnetic fault injection on SoC, with supporting experimental analysis on an actual SoC. The considerations about (a) the larger attack surface of SoCs, and (b) the fact that the speed of an SoC is not inherent mitigation are not entirely new. The former is intuitive, the latter makes sense based on existing literature on side-channel attacks (see below), and both are known to individuals skilled in the art.
The authors should consider surveying the literature around the contribution by Ghalaty et al. Differential Fault Intensity Analysis. FDTC'14.
The authors should also consider referring to a related work by Longo et al. SoC It to EM: ElectroMagnetic Side-Channel Attacks on a Complex System-on-Chip. CHES'15.

The analysis is thorough and with many examples to show exploitability. However, for an academic submission is better to show actual working attacks, even on something as simple as modular exponentiation, AES key extraction, etc. to show how attacking one of the components of the attack surface facilitates the attack.

Furthermore, because this is an applied contribution, applying the same analysis to different SoCs would help to corroborate the findings and the methodology. The authors can consider showing attacks on SoCs that perform pointer authentication, implement some form of memory integrity etc. Showing exploitation in more complex examples would help.

The section related to mitigations makes sense, that's it. Not for this submission, but given the large availability of RISC-V cores designs, the authors can consider implementing their proposed mitigations to such cores and perform an evaluation by emulating the core on FPGA.



Review #3B
===========================================================================

Overall merit
-------------
3. Weak Reject

Reviewer expertise
------------------
3. Knowledgeable

Overview
--------
In the paper the authors describe the faulting of three components of a SoC using EM fault injection. The targeted device is a broadcom BCM2837 with four A53 arm cores. 
They are capable of faulting the memory translation done by the MMU, L1 and L2 cache. The main method used to back track the origin of the faults was the JTAG debugger.

Positive aspects of the paper
-----------------------------
It is interesting to have a practical demonstration of these different components being targeted by EM fault injection. Doing these evaluations in a black box setting is a non-trivial task.

Negative aspects of the paper
-----------------------------
The paper would benefit from another round of proofreading.

Although the found fault mechanisms are interesting a more in depth discussion of the following points would be helpful for the reader:

-a clarification on how the EM-fault parameters such as pulse frequency and power were chosen. In 2.D it is stated that the frequency and
and shape of the pulse are chosen to maximize the EM-coupling. How was the coupling between probe and chip determined? 

-What was the distance between probe and target?

-When injecting EM-faults into a device there is a certain probability a fault is injected, as can be seen in figures 2 and 3. The success
rate of EM-fault injection is often rather low. The 27 tries per location feels to be on the low side to get a good idea of which parts 
of the chip are sensitive. Especially since the device is running at a rather high clock frequency which will result in a 
significant amount of timing jitter.

-In order to check whether or not the chip was faulted the output of a counter placed in a loop was checked. Faults on the instruction
level are observed when an OS was running but not on the bare metal implementation. In bare metal however only a few registers are operated
on while with the OS running the number of registers which should remain unchanged is much larger. It is therefore possible there are
instruction faults on the baremetal implementation but that they are simply not observed.

-Are all the observed faults on L1, L2 and the MMU caused by an EM-fault injection on a single location or is there a spatial separation
between the fault locations for the different components?

-Do you get the same results when the target code is executed on different cores?

-What is the success rate for faulting L1, L2 and the MMU?

-How does timing influence the outcome of faulting the different micro-architectural components? Are different instructions faulted when 
the delay is altered? Can the MMU always be faulted or only when a fault is injected inside the target code?

-In general EM-fault injection generates a fault during the storing of data because it causes a violation of the setup and hold times of for 
instance the register where the data is written to. Faulting the memory translation would thus imply that either the internal state of the MMU, the data in 
the TLBs or main memory is faulted. The text concludes based on the observed faults that the MMU is faulted directly. However the argumentation given is not 
convincing. There is a fault on the memory translation but whether it was caused by faulting the MMU or a data bus used by the MMU is unclear from the text.    

-In section 6 fault were injected in a modified version of the loop target. How was the target code modified?

-Could the fault on L2 cache not also be validated by clearing L2 cache and checking if the fault still persists?

Recommendation
--------------
Although the the authors have interesting results The results in the paper would be hard to reproduce based on the paper alone.
A more thorough description of the experimental setup and results is needed, therefore I suggest a weak reject.



Review #3C
===========================================================================

Overall merit
-------------
4. Borderline

Reviewer expertise
------------------
2. Some familiarity

Overview
--------
This work describe an EM fault injection methodology and conducts an EMFI attack campaign on a complex SOC micro-architecture. EMFI are fundamentally based on the ability of an external electromagnetic pulse to modify the electric signals propagating in the metal chip wires, generating faults. In this work, the SoC of Raspberry Pi 3 B board is selected as a target. The generated EM pulses are injected into the chip via a near field EM probe. 

The paper evaluates two major cases: of the system running with and without a Linux OS. The experimental results show that the case of OS is easier to exploit: the faults are observable at the ISA level. On the other hand, in the case of bare metal, there are no observable faults at the ISA level but rather at the uArch levels. 

The paper finds that the EM pulses could alter the programming counter values, change the virtual to physical address mapping, and shift data in cache. Given these faults, this work also suggests a countermeasure that uses message authentication codes (MACs) to ensure data integrity and authenticity.

Positive aspects of the paper
-----------------------------
This work focuses on EM fault injection on a complex SOC while most of the previous work only focused on relatively simple micro-controllers. Further, it looks at fault models with micro-architectural explanation. 

The paper is very well written. The attacks are described in detail.

Negative aspects of the paper
-----------------------------
The analysis of the effectiveness as well as the costs of the countermeasure is somewhat limited. It is critical to understand the countermeasure in light of the burden it places on the execution performance and the area/energy costs.



Review #3D
===========================================================================

Overall merit
-------------
3. Weak Reject

Reviewer expertise
------------------
2. Some familiarity

Overview
--------
This paper presents a evaluation of some electro-magnetic fault injection attacks on an SoC used in Rasbperry Pi.

Positive aspects of the paper
-----------------------------
+ interesting exploration of electro-magnetic fault injection on a recent SoC

Negative aspects of the paper
-----------------------------
- although a recent SoC is used, many features are disabled, such as only 1 CPU core is used
- like with many electro-magnetic fault injection attacks, the setup is very complex and in the reviewer's opinion unrealistic, e.g., real secure application will not give you a 'trigger' signal when to inject the fault
- the assumption that the target system is a closed box is unclear, authors know specification of the SoC, further they fully control the software, it seems authors know a lot about the SoC
- the attacks seem artificial, authors write custom software that they attack, I would expect attacks on AES at least, or another well-known cryptographic algorithm
- OS vs. no OS faults are interesting, but there isn't really any in-depth explanation, further the faults are system reboot -- a much more interesting fault is data corruption where system keeps running
- paper is written poorly with many grammar and style issues
- like with many papers, there is no information about code and hardware setup availability, the results cannot be reproduced

Recommendation
--------------
My recommendation for the paper is that authors should do further evaluation, especially, evaluate electro-magnetic fault injection against well-known software such as AES, and no custom test software.  Further, authors should explore the electro-magnetic fault injection attacks when full features of the SoC are enabled.  The current paper is interesting, but my opinion is that it is more of a report of what the authors observed, rather than giving some root causes (and solutions) for the electro-magnetic fault injection on the selected SoC.
