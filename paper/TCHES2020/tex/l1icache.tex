\section{Fault on the instruction cache}
\label{sec:icache}

In this experiment, we achieve a fault in the L1 instruction cache (L1I).

\subsection{On the impossibility to fault the instruction execution flow}

Before reporting our positive results, we must report a negative one. Contrary
to previous works on microcontrollers (where \textit{e.g.} instructions are
replaced with an \texttt{NOP} instruction), we are not able to prevent or modify
the execution of an instruction directly. Even if we cannot be
sure that no set of experimental parameters would achieve such a fault, we
thoroughly explored the parameters without success.

Instead we achieve faults during cache memory transfers.
Probably because the buses involved in these transfers are easily coupled with our EM probe.
In the case of a minimal application that can fit in the cache, the memories are filled only once at the first execution.
As a consequence, to repeatedly observe our fault, we invalidate the cache memory of interest (L1I) just before launching our application, artificially triggering a memory transfer.

\subsection{The target}

The application targeted during this experiment is two nested loops shown on
\autoref{lst:loop}, executed after the L1I cache invalidation. No fault is
obtained without the invalidation of the cache. It is built without compiler
optimizations (\verb|-O0|).

\lstinputlisting[language=Python,caption={Loop target
  application},label=lst:loop]{Figures/programs/loop.c}

% \begin{figure}[h]
%     \centering
%     \begin{subfigure}{.42\textwidth}
%     	\centering
%         \lstinputlisting[language=Python,caption={Loop target application},label=lst:loop]{Figures/programs/loop.c}
%     \end{subfigure}%
%     \vspace{0.1cm}
%     \begin{subfigure}{.55\textwidth}
%     	\centering
%         \lstinputlisting[language=C,caption={Corresponding assembly with -O0},label=lst:loopasm]{Figures/programs/loop_short.asm}
%     \end{subfigure}
% \end{figure}

Since the instruction cache is invalidated before the loop execution, the
following instructions have to be (re)loaded into the cache before their
execution. And it is this memory transfer that we target with our fault
injection. By executing the same application with and without the cache
invalidation and measuring the duration of the high state of the trigger, we
deduce that loading instructions in the cache has an overhead of
\SI{2}{\micro\second}. Our bench has a latency of \SI{700}{\nano\second}, so we
can still hit this memory transfer. To be able to observe the effect of a fault
on the full timing range, a \SI{1}{\micro\second} wait has been inserted between
the trigger and the cache invalidation.

\subsection{Forensic}

A fault occurs when the \texttt{cnt} variable value at the end of the program is
not equaled to 2500. Since a fault is detected, we use the JTAG to re-execute
our loop \texttt{for}, in the \autoref{lst:loop}, by directly setting the
\gls{PC} value at the start of the loops. Executing instruction by instruction,
we monitor the expected side effects. This execution is done without fault
injection. Each instructions has well executed except the \texttt{add}
instruction at address \texttt{0x48a08} on \autoref{lst:loopasm}.

\lstinputlisting[language=C,caption={Assembly version of the loop \texttt{for} (\autoref{lst:loop})
  without optimisation.},label=lst:loopasm,escapechar=\%]{Figures/programs/loop_short.asm}

By monitoring the \texttt{x0} register before and after the \texttt{cnt}
incrementing instruction, we observe that the value is kept unchanged: the
increment is not executed. Since the fault is still present after the EM
injection, we can conclude that a wrong instruction value is stored in L1I. We
confirm this fault model by executing a L1I cache invalidation instruction
\texttt{ic iallu}\footnote{We set the \gls{PC} value to the \texttt{ic iallu}
  instruction address in memory.}. Re-executing our application, the fault has disappeared.

We can infer that the injected fault has affect a part of the L1I cache.
However, it is impossible to access (read) the new incremented value. Since the
fault happens during the cache filling, we can suppose that it is the memory
transfer had been altered.

Moreover, this fault model matches the observation made by \textit{Trouchkine et
  al.} in \cite{conf/wistp/TrouchkineBC19}. In this work, the authors
characterize a fault model while perturbing a BCM2837 using \gls{EMFI}. The
model they observe is the corruption of the executed instruction second operand
and which is forced to \texttt{0}. This \gls{ISA} fault model matches the one we
observe in the cache as the corruption of the instruction \texttt{add x0, x0,
  \#0x1} by forcing the second operand to \texttt{0} gives the instruction
\texttt{add x0, x0, \#0x0} which explains why the increment is not executed as
we add \texttt{0} to the register. Also, the identify that their fault happens
during the instruction fetch, matching with a fault in the cache as we observe
it.

\subsection{Exploits}

This fault is one of the easiest to exploit since it is similar to the classical
instruction skip model. Therefore, most exploits based on this classical model
apply here. Since the faulted value is still present in the cache, it will stay
faulted until the cache is invalidated: we can call this model ``sticky
instruction skip''. Bukasa \textit{et al.}~\cite{Bukasa2018} demonstrate
applications of this fault model: hijacking the control flow and initiating a
\gls{ROP} attack among others.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
