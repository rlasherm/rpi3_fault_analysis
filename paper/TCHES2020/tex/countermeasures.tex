\section{Defence mechanisms}
\label{sec:countermeasures}


In this paper, we demonstrated that the hardware cannot be relied upon for
critical functionalities. Our forensic analyses show that the memory hierarchy
and the \gls{MMU} can easily be subverted. But it is probable that other
micro-architectural blocks can also be targeted. We can conjecture that
blocks ensuring data coherence, speculative execution, \textit{etc.} will
one day be the victims of fault attacks.

As such, protecting a \gls{CPU} against fault attacks requires to ensure the
integrity of their micro-architectural blocks. If this problem is well known for
secure chips such as smartcards, hardening the specific block that
constitute a modern and complex \gls{CPU} is an open problem.\\

In this section, we discuss on how protect complex \gls{CPU} to fault injection
attacks inside the \gls{CPU} or out of it through the \gls{SoC} organisation.

\subsection{Inside the \gls{CPU}}
\label{sec:inside-glscpu}

\subsubsection{Ensuring integrity and authenticity in cache memories}


Instead of solving the integrity problem for all micro-architectural blocks, we propose to focus on the cache memories.

Our solution is to add redundancy in each memory in the form of one \gls{MAC} per cache line.
Indeed safety-oriented protection such as error correcting codes are not designed to resist a malevolent attacker.

A generic solution is to use a \gls{KMAC} with a small output length, since the lifetime of the integrity and authenticity protection is limited and to reduce the memory overhead.
With an output length of 64 bits (we store \SI{8}{\byte} of MAC for each \SI{64}{\byte} cache line), the protection requires $12.5\%$ more memory than the unprotected version (without taking into account tags and flags).

\paragraph{Key generation}

The key $k$ is a true random value that the system should generate at boot time.
It must be kept secret, so that the attacker cannot know the correct \gls{MAC} value for a given cache line.

\paragraph{Signing}

For each cache line modification, total or partial, the \gls{KMAC} must be computed again.
For a cache line tag $t$, data block $d$ and flags $f$, let $h = KMAC(k, t|d|f)$ be the \gls{MAC} value stored along the cache line.
Instead of the tuple $(t, d, f)$, we now store $(t, d, f, h)$.

\paragraph{Verifying}

When we need to read a cache line, we have to verify the \gls{MAC}.
For a read in the cache line consisting in the tuple $(t, d, f, h)$, we compute $h_{verif} = KMAC(k, t|d|f)$.
Finally, we compare $h$ and $h_{verif}$. If they are identical, we proceed with the read. Otherwise, we raise an alarm.

\subsubsection{The path to secure cores}

Complex cores today are mainly split into two classes, the high performance ones
and the low energy consumption ones. The security problems are addressed through
software modification (\textit{e.g.} retpoline against the spectre attack) or
minimal hardware modification (\textit{e.g.} TrustZone to protect against rich
\gls{OS} vulnerabilities). Security is considered as an implementation issue,
where attacks happen only if a designer made a mistake.

But this work shows that a modern complex core is not a reliable substrate for
secure computations. Generic secure cores must be designed to handle all the
security needs. These secure cores will have to be designed from the ground up,
to guarantee security properties across all micro-architectural blocks. We
envision a future where the \glspl{SoC} will be composed of $3$ kinds of cores:
a core can either grant high performance, low energy consumption or high
security.

\subsection{Though the \gls{SoC} organisation}

% \subsubsection{3D packaging}

It is worth mentioning a natural countermeasure that may complicate fault
attacks on \glspl{SoC}, 3D packaging. This technique consists in stacking the
dies (CPU, memory, baseband, etc.) in the same package. Experimentally, the
attacker may find it difficult to target dies that are not on the surface of the
package. Yet it has been demonstrated possible: for example, Vasselle \textit{et
  al.}~\cite{Vasselle2017} show a laser fault attack on a \gls{SoC} where the
external RAM is stacked on it.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
