\section{Introduction}

% From a world where each application has its dedicated hardware support (high
% performance chips for desktop computers, low-power chips for mobile phones,
% hardened chips for smartcards, \textit{etc.}), more and more applications
% are today executed on the same device: the smartphone. They are powered by fast and
% low-power components, the \glspl{SoC}, where a set of modules (baseband, \gls{GPU}, Flash memory, \textit{etc.}) takes place on the same silicon
% layout as the \gls{CPU}.

Today, complex \glspl{SoC} are used for a lot of applications, even sensitive
ones which are usually done by dedicated hardware such as smartcards. Despite
many modules compose a \gls{SoC}, in this work we only focus on their \gls{CPU}.
The reason is that, compared to \glspl{MCU}, these \glspl{CPU} have an intrinsic
complexity which can lead to potential security flaws \cite{SPECTRE,MELTDOWN}.

With the democratization of smartphones, \glspl{CPU} manipulate both sensitive
and non-sensitive processing. Aware of the aforementioned risk, vendors invest a
lot of work into the software layer hardening with numerous security mechanisms
(\gls{TEE}, secure boot, memory partitioning, etc) embedded into the \gls{OS}.
However, the hardware layer misses the same attention: specifically, modern
\glspl{CPU} are not protected against physical attacks, especially against
\gls{FI} attacks.

\gls{FI} attacks constitute a well-known class of attacks where an attacker
modifies the physical environment of the targeted chip to induce a fault during
its execution. The resulting failure can be used to extract sensitive
information or bypass an authentication for instance. More generally, \gls{FI}
attacks give the ability to perturb a program execution, defeating static
countermeasures that cannot foresee the failure (\textit{e.g.} secure boot,
access control).

% The attacker’s ability to subdue the system is highly dependant of its
% experimental capacities: fault injection attacks can be performed with power glitches,
% clock glitches, lasers, electromagnetic pulses, \textit{etc.}

In this paper, we propose an \gls{EMFI} effects characterization and an attack
against the \gls{CPU} of a BCM2837 from a Raspberry Pi 3 model B board. This
board is widely used for prototyping mobile devices and has an important
community, making it an interesting target to work on. The characterized faults
are exploited to realize a \gls{PFA} on an AES.

% Electromagnetic pulses modify the electric signals in the chip's metallic wires.
% Faults are generated when signals are modified during a small-time window around
% the clock rising edge. In this case, according to Ordas \textit{et
%   al.}~\cite{OrdasGM15}, a faulty value may be memorized.

% Most previous \gls{EMFI} attacks target microcontrollers. Indeed, these chips are
% slower and simpler than \glspl{SoC}. Therefore, an attacker can easily disrupt
% microcontrollers and exploit a fault.

\subsection{Motivation}
\label{sec:motivation}

The \gls{CPU} security model is focused almost entirely on software, so much
that hardware countermeasures such as \glspl{TEE} explicitly exclude hardware
attack path from their vulnerability analysis. However, \gls{FI} attacks on
modern and complex \glspl{CPU} is a recent research topic where some published
articles \cite{TimmersSW16,BADFET,MajericBB2016} focus on breaking software
security properties.

\glspl{FI} allows to modify a program behaviour at runtime and therefore create
vulnerabilities in a sound (and even proved correct) software. The stake is the
system security as a whole, as in controlling what software is executed and what
data can be accessed by whom. With software security constantly improving and
the costs and experimental difficulty of performing fault injection attacks
declining, we can surmise that \glspl{FI} will become a major threat for mobile
devices in the future.

Therefore, we think that understanding the \gls{FI} effects on complex \gls{SoC}
is an important challenge.

% Understanding the \gls{EMFI} effects on a \gls{SoC} is required to understand
% the threat and to design effective countermeasures. There is an extensive
% literature on fault injection attacks on microcontrollers, and as a result, the most
% secure devices against them are derived from microcontrollers (aka secure
% components). The same work has to be done for complex \glspl{CPU}.

\input{tex/previous}

\subsection{Contributions}
\label{sec:contribution}

In this article, we focus on an ARMv8 \gls{SoC}, namely the Broadcom BCM2837 chip
at the heart of the \gls{RPi3}. It is a widely successful low cost single
board computer. This quad-core Cortex-A53 \gls{CPU} runs at
\SI{1.2}{\giga\hertz} and features a modern ``smartphone class'' processor. We
are using \gls{EMFI} and observe the resulting failures to deduce their origins. 

We observe radically new fault models that are neither described in other works
nor taken into account when discussing the security of modern embedded systems.
In this paper, we demonstrate how we recovered these fault models and provide insights on the micro-architectural mechanisms leading to these models. 

The consequences are dire: a \gls{SoC} must not be considered as a black box
with respect to security. It is not enough to work on the software side security
if it does not rely on solid hardware foundations.\newline

The goal is to provide a micro-architectural explanation of the observed
behaviour. To that end, we have to control the targeted system and limit its
complexity. It implies most notably that we use a single-core configuration and
setup an identity mapping for virtual memory. This simplification choice does
not imply necessarily a harder exploitation on more realistic systems, since the discovered fault models would still be present.
But on such systems, it becomes hard to isolate the effect of a fault and attribute it to one subsystem: we cannot propose a simple model explaining the observed behaviour.

We describe our setup in
\autoref{sec:fault_injection}, both the experimental apparatus used to inject faults but
also the targeted hardware and software environment.

Observed faults on a bare metal system are analysed in sections \ref{sec:icache},
\ref{sec:mmu} and \ref{sec:l2cache}. For each fault category, we explain
the process that allows us to infer the cause of the failure. The possibility to
exploit these faults will be discussed as well as the experimental difficulties
to achieve them.

A fault exploitation demonstration is given in \autoref{sec:exploitation}, where an AES cryptographic key is recovered with one fault injection and $2^{24}$ hypotheses.

We finish with propositions to protect \glspl{SoC} against
these attacks in \autoref{sec:countermeasures} and conclude in
\autoref{sec:conclusion}.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
