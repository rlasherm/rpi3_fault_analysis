\section{Introduction}
\label{sec:introduction}

Sensitive operations usually done on secure devices, such as smartcards, are
more and more realized on more powerful and complex ones: the \glspl{SoC}. These
\glspl{SoC} are powered by \glspl{CPU} with a complex micro-architecture
featuring memory virtualization, several cache levels, speculative or
out-of-order execution.

As these complex \glspl{CPU} are tuned for the best performance/energy
consumption trade off, their security mainly relies on software mechanisms.
However, past works have shown that physical attacks are effective against them,
in particular fault attacks where the attacker modifies the physical environment
of a chip to alter the computation inside. These vulnerabilities demonstrate the
need for hardening complex \glspl{CPU} against fault attacks. To achieve that
goal, we first need to understand the effect of such faults on the
micro-architecture.

Some recent papers have proposed the characterization of faults on complex
\glspl{CPU}. However, since they use a device running a complex \gls{OS}, their
analysis cannot precisely infer the impact on micro-architectural elements. In
this work, we propose instead to run our open-source characterization software
as a bare metal application for the \gls{RPi3} \gls{CPU}. This implementation
allows us to realize a precise characterization of the \gls{EMFI} fault effects
on several micro-architectural components of the \gls{CPU}.

We demonstrate persistent faults in the L1 instruction cache, data cache or L2
cache, allowing a sticky instruction skip fault model. In addition, we show that
the \gls{MMU} can be tampered with to modify the virtual-to-physical memory
mapping.

Finally, we prove the potency of a fault in the L1 data cache by implementing a
\gls{PFA} to retrieve a cryptographic key on an AES implementation.

Due to the \gls{CPU} specificities, these fault models are different from the
ones usually observed on \glspl{MCU}, implying specific countermeasures and
confirming the need for additional fault characterization on complex
\glspl{CPU}.

\subsection{Related works}
\label{sec:related_works}
Since the first practical fault attack in 2002 \cite{SkorobogatovA02}, a lot of
work has been spent to demonstrate how fault attacks can break security
mechanisms, mostly cryptography~\cite{SchmidtH08,WoudenbergWM11,YuceGSDPS16}.

Inspired by these papers, fault attacks targeting complex \glspl{CPU} have
emerged. They target the secure boot~\cite{TimmersSW16,Vasselle2017,BADFET}, the
\gls{TEE}~\cite{TangSS17}, the memory partitioning~\cite{drammer} or some
\gls{OS} security mechanisms~\cite{TimmersM17}.

Among all the fault injection methods, \gls{EMFI} has been demonstrated to be
efficient on complex \glspl{CPU} with a minimal effort on the target
preparation~\cite{BADFET}. Regarding this injection medium, if a lot of fault
characterization works have been done on
\glspl{MCU}~\cite{MoroDHRE13,RiviereNRDBS15}, only few focus on complex
\glspl{CPU}~\cite{proy2019,conf/wistp/TrouchkineBC19}.

In \cite{proy2019}, the authors develop a fault model at the \gls{ISA} level. In
other words, they identify how the data and the instructions of a program are
affected by the fault. This knowledge is important for understanding the impact
on a program and to propose software countermeasures, but some works have shown
that software-based countermeasures are not sufficient~\cite{YuceGSDPS16}.
However, for building hardware countermeasures, the fault model must be
characterized at the micro-architectural level, not the \gls{ISA} level.

To complete this approach, Trouchkine \textit{et
  al.}~\cite{conf/wistp/TrouchkineBC19} propose a model for complex \glspl{CPU}
and a method which aims at obtaining micro-architectural information about the
fault from observations at the \gls{ISA} level. Using this method, the authors
are able to identify that the faults are appearing in the registers, the
pipeline or the cache of an x86 and an ARM \glspl{CPU}. Even if this method is
able to identify the affected micro-architectural blocks of the \gls{CPU}, due
to the complex \gls{OS} presence and the \gls{ISA} point of view, they are not
able to precisely determine the micro-architectural fault model.

\subsection{Motivations}
\label{sec:motivations}

\gls{EMFI} is an efficient way to tamper with a complex \gls{CPU}, to create a
vulnerabilities in a software (even if it is analysed or proved), but the effect
of a fault on the micro-architecture is not known precisely. Unfortunately,
hardware countermeasures are difficult to develop without this knowledge.

The key limiting factor in previous works is the presence of a complex \gls{OS}.
Therefore, we propose a method to infer the micro-architectural fault model with
bare metal applications, and we carry this characterization on a \gls{RPi3}’s
\gls{CPU}.


%we aim at providing a first micro-architectural knowledge about \gls{EMFI} effects on these devices and a method for carrying this characterization to other devices.
% Also, to be as close as possible to the micro-architecture, we want to avoid the
% presence of a complex \gls{OS}.

\subsection{Contribution}
\label{sec:contribution}

In this work, we introduce bare metal characterization software to infer
micro-architectural fault models on a \gls{RPi3}\footnote{This implementation
  and the experiment data are released as open-source software (MIT Licence)
  here: \textbf{blinded for reviews}.}, using a forensic process based on the
JTAG debug port.


We describe our setup in \autoref{sec:fault_injection}, both the experimental
apparatus used to inject faults but also the targeted hardware and software
environments.

Observed faults on a bare metal system are analysed in
sections~\ref{sec:icache}, \ref{sec:mmu} and \ref{sec:l2cache}. A persistent
fault in the L1 instruction cache is described in \autoref{sec:icache},
tampering with the virtual-to-physical memory mapping is shown in
\autoref{sec:mmu} and a persistent fault in the L2 cache is demonstrated in
\autoref{sec:l2cache}. For each fault category, we explain the process that
allows us to infer the cause of the failure. The possibility, and the
limitations, to exploit these faults will be discussed as well as the
experimental difficulties to achieve them. A fault exploitation demonstration is
given in \autoref{sec:exploitation} with a persistent fault in the L1 data
cache, where an AES cryptographic key is recovered with one fault injection and
$2^{24}$ hypotheses.

We finish with propositions to protect complex \glspl{CPU} against these attacks
in \autoref{sec:countermeasures} and we conclude in \autoref{sec:conclusion}.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
