\section{Fault injection on embedded systems}
\label{sec:fault_injection}

%TODO faulted JTAG ?

\subsection{The targeted chip: The BCM2837 on the Raspberry Pi 3 B}
\label{sec:setup}

\subsubsection{Presentation}

The \acrfull{RPi3} is a low-cost single-board computer. It features a complete
system able to run a complex OS such as Linux or Windows and their applications.
The \gls{SoC} powering this board is the BCM2837 from Broadcom, a
\SI{28}{\nano\meter} quad-core Cortex-A53 \gls{CPU} running at
\SI{1.2}{\giga\hertz} with the help of a dual-core VideoCore IV GPU at
\SI{400}{\mega\hertz}. This \gls{SoC} also features \SI{512}{\kilo\byte} of cache
memory and various wireless or contact connections such as HDMI, Wi-Fi,
ethernet, USB, etc.

Our experiments are performed with our own software stack\footnote{Available on
  \textbf{blinded for reviews}.}, with only one core active. Indeed, even if we
tested \gls{EMFI} with several cores enabled, it is hard to pinpoint the exact
effect on the micro-architecture in this configuration since we cannot easily
link a fault in unified memory with a particular core. Therefore, to increase
our confidence in our characterization, we enable only one core.

To control the behaviour of the chip, we have implemented the minimum to run our
applications: initialization of JTAG, UART, GPIO, \gls{CPU} caches and
\gls{MMU}. We want to stress out that no complex \gls{OS} is running during our
experiments in sections~\ref{sec:icache}, \ref{sec:mmu}, \ref{sec:l2cache} and
\ref{sec:exploitation}, to avoid interference that could hinder our ability to
infer more precise the fault models. In particular, we try to avoid the effects
of context switching due to preemptive scheduling by the \gls{OS}, the error
recovery mechanisms (if an error occurs, we want to know it) and the caches
maintenance performed by the \gls{OS}.

In order to later explain the causes of the failures observed, we describe in
more details two important micro-architecture blocks of this complex \gls{CPU}:
the cache hierarchy and the \gls{MMU}.

\subsubsection{Cache hierarchy}

In modern systems, memory accesses are a lot slower than the \gls{ALU}. To avoid
losing too much performance to this latency difference, small and fast memories
called caches are used to mirror a part of the memory space.

First, in the targeted \gls{CPU}, each core has two L1 caches (the smallest and
fastest kind), one dedicated to instructions (L1I), one to data (L1D). These
caches are \SI{16}{\kilo\byte} in size with \SI{64}{\byte} line width.

Second, a second layer of cache memory, the L2 cache, is common to all cores and
thus provides a unified view of the memory space. Its size is
\SI{512}{\kilo\byte} with \SI{64}{\byte} line width.

The cache organisation is displayed on \autoref{fig:memhier}.


\begin{figure}[h]
  \centering
    \includegraphics[width=0.3\textwidth]{Figures/BCM2837_caches.png}
    \caption{Memory hierarchy for the BCM2837.}
    \label{fig:memhier}
\end{figure}

% \begin{figure}
% 	\centering
% 	\includegraphics[width=0.5\textwidth]{Figures/BCM2837_caches.png}
% 	\caption{Cache memory hierarchy\label{fig:caches}}
% \end{figure}

% \paragraph{\gls{MMU}}

% \TODO{Peut être redondant avec la section rajoutée par Thomas}

% Our system bases its memory isolation solution on the virtual memory
% abstraction. Any process works as if the whole memory space is available to it.
% Behind the scene, the process uses virtual addresses that are mapped, on the
% fly, to physicall addresses. The mechanism used to ensure that this mapping is
% done correctly and with a low latency is a mix of software and hardware system.

% On the software side, the \gls{OS} (when there is one), is responsible for
% specifying the mapping: \textit{i.e.} which physical address is mapped from some
% virtual one. This mapping is stored in the page tables, a memory section that
% specify for each page (here a 64kB virtual memory chunk) the correponding
% physical address as well as access rights assotiated to it. In a standard
% \gls{OS}/applications setting, each process has its dedicated page tables which
% ensure a strong form of memory isolation between processes. In our simple
% implementation, we have a single application. The \gls{MMU} is enabled but with
% an identity mapping: physical addresses and virtual ones are identical. One has
% to note that the \gls{MMU} has to be enable to run a program: instructions
% alignment requirements are not the same with or without the \gls{MMU} enabled and
% our compiler (Linaro GCC 6.2-2016.11) requires the \gls{MMU}.

\subsubsection{\gls{MMU}}

The \gls{MMU} is a central component for every multi-applications system. It
aims for virtualizing the physical memory of the system into a virtual one to
segregate each apps in their context. Therefore, the \gls{CPU} only works with
virtual addresses and during a memory access to one of them, the
\gls{MMU} translates it into the corresponding physical address which is
transmitted to the memory controller. The information required for the
translation of an address is called a \gls{PTE} and it is stored in the physical
memory and cached in the \gls{TLB}. There is a \gls{PTE} for all allocated pages
in the physical memory. Our bare metal implementation allocates the whole
address space with an identity mapping (virtual and physical addresses are the
same) with \SI{64}{\kilo\byte} pages.

In modern systems, the translation phase does not only compute the physical
address but also realizes different checks. These checks are monitoring if the
page can be written or not, which kind of process (user or supervisor) can
access it or should the page be stored in cache or not.

Among all its roles, the \gls{MMU} is also a security mechanism. Ensuring that
a read-only page cannot be written to and ensuring that only authorized processes
can access their corresponding pages. This last security mechanism is the memory
partitioning. On multi-applications systems, it avoids a process to spy or
corrupt the memory area used by another one.

On complex \glspl{OS}, the \gls{MMU} and the \glspl{PTE} are critical assets set
up by the kernel.


% \paragraph{Memory coherence}

\subsection{The Electromagnetic fault injection bench}%: Faustine}

To inject faults on the BCM2837, some apparatus is required. Our experimental
setup has been designed to be highly configurable and to work at higher
frequencies than most setups targeting microcontrollers. First, we use a
Keysight 33509B (waveform generator) to control the delay between a trigger
issued by the \gls{RPi3} board before the instructions of interest. Second, a
Keysight 81160A generates the signal shape for the EM injection: one sinus
period. % at \SI{275}{\mega\hertz} with a power of \SI{-14}{\dBm}.
A sinus is used instead of the usual pulse since it gives fewer harmonics at the
output of the signal generation chain. Third, this signal is amplified with a
Milmega 80RF1000-175 (\SI{80}{\mega\hertz} - \SI{1}{\giga\hertz} bandwidth).
Finally, the high-power signal is connected to a Langer RF U 5-2 near-field
probe, in contact with the packaged chip. A part of this energy is therefore
transmitted into the metallic lines of the chip, which can lead to a fault.

The minimum latency between the initial trigger and the faulting signal reaching
the target is high: around \SI{700}{\nano\second}. As a consequence, the
targeted application must run enough time to be reachable by our fault injection
bench.

\subsubsection{Bench parameters}

To properly inject a fault, the experimenter must tune several parameters,
namely the
\begin{inparaenum}[1)]
  \item injection signal,
  \item the probe spatial location and
  \item temporal synchronisation.
\end{inparaenum}

Fault effects are reproducible with a low ratio; meaning that if a fault has
been achieved, it will be achieved again with the same parameters but only for a
small ratio of the fault injections. In the other cases, no failure occurs or
another effect is observed (mostly due to jitter).
Unfortunately, we are not able to measure the fault ratio. Indeed a manual forensic analysis must be performed in order to establish the cause for a fault.
Since this step is not automatized, the fault ratio cannot be measured.

\paragraph{Injection signal} The signal parameters (shape, frequency, number of periods)
have an optimal value with respect to our requirements:

\begin{itemize}
  \item The shape is a sinus to reduce the presence of harmonics in the
    amplification chain.
  \item The frequency (\SI{275}{\mega\hertz}) maximizes the probe gain as
    measured in the probe characterization phase.
  \item The amplitude (\SI{-14}{\dBm}) has been manually tuned during the fault
    injection until the desirable effect is achieved: if too low, no faults are
    observed; if too high, the chip crashes and must be rebooted.
\end{itemize}



\paragraph{Probe spatial location}

To find the sensitive locations on the chip, a sensitive map has been performed.
Setting a high signal amplitude, we repeatedly try to inject a fault on all
possible locations on the chip. Then, we observe where faults (the application
under test, \autoref{lst:loop}, sends back an incorrect result or no result at
all) are obtained. The result, displayed on \autoref{fig:rpi3_carto_bare_metal},
clearly indicates an area of interest.

\begin{figure}[h]
  \centering
  % \includegraphics[width=.5\textwidth]{Figures/raspi3_carto/baremetal_reboot_scatter.pdf}
  \includegraphics[width=.5\textwidth]{Figures/raspi3_carto/baremetal_bcm2837_carto_scatter.pdf}
  \caption{Bare metal sensitivity map}
  \label{fig:rpi3_carto_bare_metal}
\end{figure}


For all experiments below, the probe is placed at the most sensitive location ($X=\SI{4}{\milli\meter}$, $Y=\SI{4.5}{\milli\meter}$ on \autoref{fig:rpi3_carto_bare_metal}).


\paragraph{Temporal synchronization}

The main difficulty for fault injection is the temporal synchronization: when to inject
precisely a fault on the targeted and vulnerable instructions.

We first need a temporal reference, given here by a GPIO: an
electrical rising edge is sent to a board pin by our application just before the
area of interest. This signal is used by our apparatus to trigger the fault injection.

Our setup is using the evaluator approach: the attacker can instrument the system to ease the experiments. In the case of a
real attack, the adversary would have to generate this trigger signal: it can be
done by monitoring communications, IOs, or EM radiation to detect patterns of
interest. In all cases, it is a tricky business highly application dependant.

The trigger signal is just part of the problem: from this instant we must
wait the correct duration to inject the fault.
In other words, we want to control the injection time relatively to the trigger signal, to inject a fault during the targeted instruction processing.
Propagation times are not negligible. On a modern
\gls{CPU}, the matter is made more difficult by the memory hierarchy. Since
cache misses are highly unpredictable, they imply a corresponding jitter. It is
hard to precisely predict the duration of a memory access and therefore the time
to wait to inject the fault.

Synchronization is a problem, but not a hurdle that much. Indeed, the
attacker only has to inject faults until the correct effect is achieved. Because
of the jitter, for the same delay (time waited between trigger and fault injection), different timings are tested with respect to the running program.
For the same bench timing parameters, we will inject a fault during a different instruction at each execution, but a different one among a small set of instructions, depending of the jitter.
If a fault with an interesting effect is possible, it is eventually achieved.

Additionally, as we will see in the next sections, memory transfers are
particularly vulnerable to \gls{EMFI}. They are also slower than the core
pipeline, allowing for a bigger fault injection timing window.

\subsection{Forensic methodology}

Contrary to state-of-the-art fault model characterization on complex
\glspl{CPU}, introduced in \autoref{sec:related_works}, we target a bare metal
system, meaning that no complex \gls{OS} such as Linux is present. Indeed we try
to characterize the effect of a fault in the micro-architecture, since this is
the only abstraction level where meaningful defence mechanisms can be added. The
presence of a complex \gls{OS} would modify the fault model and make it harder
to link to the micro-architecture. As an example, previous works have reported
successful instruction skips with an \gls{OS} present, a fault model that we are
not able to reproduce with a bare metal application. Is it because of
differences in the experimental setups? Or because the \gls{OS} transforms the
memory hierarchy faults described in this paper into instruction skips thanks to
cache maintenance, transparent to \gls{OS} applications?


As the targeted system is closed (we do not have any insider information), we
have limited means to explore what is happening in the system, mainly the JTAG.
With it, we are able to halt the chip execution to read the register values and
to read and write memory as seen by a particular core (with a data viewpoint).
Therefore, to pinpoint the particular effects of a fault injection, we force the
system state such that an observable change gives us information on the fault
mechanism. To maintain controllability on the system state, our software
footprint has to be minimal. As such we will not describe how to breach a
particular system with our faults since any exploit is highly application
dependant and our setup is not representative of a standard application
environment. Instead we will suggest exploit strategies: how such faults could
be used by a malicious attacker?

A demonstration of a fault exploitation, given in \autoref{sec:exploitation}, is
done on our own AES encryption implementation. However, those results cannot be
extrapolated to any real-world application.
 
With this setup, we will identify different fault models with EM fault injection: the L1I cache in \autoref{sec:icache}, the MMU in \autoref{sec:mmu}, the L2 cache in \autoref{sec:l2cache} and the L1D cache in \autoref{sec:exploitation}.

% \subsection{Targeted application}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% default-justification: left
%%% ispell-dictionary: "english"
%%% TeX-command-extra-options: "-shell-escape"
%%% TeX-master: "../main"
%%% End:
