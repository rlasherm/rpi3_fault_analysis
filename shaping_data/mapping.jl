
using DataFrames

function ind_range(val::Float64, r::LinRange)::Int64
    t = (r.stop - r.start) / r.lendiv
    approx = (val - r.start)/t

    if approx < 1
        return 1
    elseif approx > r.len
        return r.len
    else
        round(Int64, approx)+1
    end
end

function points2map(points::DataFrame, xrange::LinRange, yrange::LinRange)
    m = zeros(Int64, xrange.len, yrange.len)

    for p in eachrow(points)
        xind = ind_range(p.x, xrange)
        yind = ind_range(p.y, yrange)
        m[xind, yind] += 1
    end

    return m
end