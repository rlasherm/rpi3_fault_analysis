
#[macro_use] extern crate failure;
extern crate regex;
#[macro_use] extern crate lazy_static;

use std::io::prelude::*;
// use std::net::TcpStream;
use std::io::{BufReader, BufWriter};
use std::process::{Command, Stdio};
use std::{thread, time};
use std::fs::File;
use std::env;


use regex::Regex; 
use failure::Error;

const I1: &str = "0xd50e7800";
const I2: &str = "0xd5387400";
const I3: &str = "0xd50e871f";
// const ADD1: &str = "0x0200000";
// const ADD2: &str = "0x0200004";
// const ADD3: &str = "0x0200008";
const ADD1: &str = "0x7000";
const ADD2: &str = "0x7004";
const ADD3: &str = "0x7008";
const EOL: &str = "\r\n";
const ESC: u8 = 255;

lazy_static! {
        // static ref PARSE_REG: Regex = Regex::new(r"[[:alnum:]]+ (/64): 0x([0-9]+)> ").unwrap();
        static ref PARSE_REG: Regex = Regex::new(r"0x([0-9A-Fa-f]+)").unwrap();
    }

fn main() -> Result<(), Error> {

    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("Usage: read_mmu_map [OUTPUT_FILE]");
        return Ok(());
    }

    let filename = args[1].to_string();
    let file = File::create(filename)?;
    let mut file_writer = BufWriter::new(file);
    
    // let jtag = TcpStream::connect("127.0.0.1:4444")?;
    // let mut writer = BufWriter::new(jtag.try_clone()?);
    // let mut reader = BufReader::new(jtag.try_clone()?);
    let telnet = Command::new("telnet")
                    .args(&["localhost", "4444"])
                    .stdin(Stdio::piped())
                    .stdout(Stdio::piped())
                    .spawn()
                    .map_err(|e|format_err!("Cannot launch telnet: {}", e))?;
    let mut writer = telnet.stdin.ok_or(format_err!("Cannot access telnet stdin"))?;
    let mut reader = BufReader::new(telnet.stdout.ok_or(format_err!("Cannot access telnet stdout"))?);
    

    read_to_next(&mut reader)?;

    //halt
    write(&mut writer, "halt")?;
    read_to_next(&mut reader)?;

    //write the correct instruction @ ADD1
    write_inst_at(&mut writer, I1, ADD1)?;
    read_to_next(&mut reader)?;
    write_inst_at(&mut writer, I2, ADD2)?;
    read_to_next(&mut reader)?;
    write_inst_at(&mut writer, I3, ADD3)?;
    read_to_next(&mut reader)?;

    //read map
    let mut va = 0x0;
    let step = 0x10000;

    for _ in 0..0x100 {
        let (pa, cache, flags) = read_map(&mut reader, &mut writer, va)?;
        writeln!(file_writer, "0x{:x} -> 0x{:x}, {:x}, {:x}", va, pa, cache, flags)?;
        println!("0x{:x} -> 0x{:x}, {:x}, {:x}", va, pa, cache, flags);
        va += step;
    }

    Ok(())
}

fn write<T: Write>(stream: &mut T, s: &str) -> Result<(), Error> {
    // print!("Writing \"{}\"...", s);

    let news = s.to_owned() + EOL;
    match stream.write(news.as_bytes()) {
        Ok(_) => { /*println!("Done");*/ },
        Err(_e) => {/*println!("Fail: {}", e);*/}
    }

    let wait_time = time::Duration::from_millis(50);
    thread::sleep(wait_time);
    
    Ok(())
}

fn write_inst_at<T: Write>(stream: &mut T, inst: &str, at: &str) -> Result<(), Error> {
    write(stream, &("mww ".to_owned() + at + " " + inst))
}

fn read_map<W: Write, R: BufRead>(reader: &mut R, writer: &mut W, address: u64) -> Result<(u64, u8, u16), Error> {//(address, cache type, flags)
    let add_str = format!("0x{:x}", address);
    //set pc
    write(writer, &("reg pc ".to_owned() + ADD1))?;
    read_to_next(reader)?;
    //set address
    write(writer, &("reg x0 ".to_owned() + &add_str))?;
    read_to_next(reader)?;

    //execute 2 instructions
    write(writer, "step")?;
    read_to_next(reader)?;
    write(writer, "step")?;
    read_to_next(reader)?;

    //read mapping in x0
    write(writer, "reg x0")?;
    let raw_mapping = read_to_next(reader)?;
    // let x0_str = parse_register(raw_mapping);
    // let x0 = u64::from_str(&x0_str).map_err(|e|format_err!("Cannot parse {}: {}", x0_str, e))?;
    let x0 = parse_register(&raw_mapping)?;

    let cache = ((x0 & 0xFF00000000000000) >> 56) as u8;
    let flags = (x0 & 0xFFF) as u16;
    let add = x0 & 0x00FFFFFFFFFFF000;

    Ok((add, cache, flags))
}

fn parse_register(raw: &str) -> Result<u64, Error> {
    let captured = match PARSE_REG.captures(raw) {
        Some(capts) => { capts[1].to_string() },
        None => {return Err(format_err!("Cannot capture register value for {}", raw)); }
    };

    let u = u64::from_str_radix(&captured, 16)?;
    Ok(u)
}

fn read_to_next<T: BufRead>(stream: &mut T) -> Result<String, Error> {
    let mut vec = Vec::new();
    let mut buf = [0u8; 64];

    let end_seq: Vec<u8> = vec![0x3E, 0x20];
    let seq_size = end_seq.len();
    let mut current = 0;
    let mut escape = false;

    let mut end = false;

    while end == false {
        let bytes_count = stream.read(&mut buf)?;

        for i in 0..bytes_count {
            if buf[i] == end_seq[current] {
                current += 1;
            }
            else {
                current = 0;
            }

            if buf[i] != ESC {
                if escape == true {
                    escape = false;
                }
                else {
                    vec.push(buf[i]);
                }
            }
            else {
                escape = true;
            }
            
            if current == seq_size {
                end = true;
                break;
            }
        }
    }
    
    let s = String::from_utf8_lossy(&vec).to_string();
    // println!("Read: {}", s);
    Ok(s)
}

#[test]
fn test_regex() 
{
    let to_test = "x0 (/64): 0xCC00000002020980";
    println!("Captured {:?}", PARSE_REG.captures(&to_test));
}