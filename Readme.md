# EM fault injection on Raspberry Pi 3

The paper can be found [here](https://hal.archives-ouvertes.fr/hal-03175704).



This repository contains the source code, experimental data and paper for our EM fault injection on the Raspberry Pi 3B.

The target code with the loop can be found in [code/RPi3_loop](./code/RPi3_loop). The resulting experimental data are [code/RPi3_loop/experiments](./code/RPi3_loop/experiments).

The target code and data for the PFA attack on AES can be found in [code/RPi3_AES](./code/RPi3_AES).
