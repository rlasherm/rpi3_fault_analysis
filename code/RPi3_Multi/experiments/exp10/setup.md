
0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


# Target

Loop

# Settings

X = -6mm
Y = 2.5mm

Delay = 800ns

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm

# Qualitative observations



git diff --no-index --word-diff=color --word-diff-regex=. ref.log diff0.log