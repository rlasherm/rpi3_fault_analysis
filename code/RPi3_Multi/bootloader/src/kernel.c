/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "init.h"
#include "jtag.h"
#include "mem.h"
#include "interrupt_handler.h"
#include "boot.h"
#include "secure_boot.h"
#include "mmu.h"
#include "myaes.h"
#include "fault.h"
#include "mutex.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// #include "mbedtls/error.h"

#define DATA_SIZE 16

unsigned int cores_activation = 0;
char text[256];

void gpio_init() {
  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}

void initialise_monitor_handles(void)
{

}

void boot_init ( void )
{

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  // call construtors of static objects
	extern void (*__init_start) (void);
	extern void (*__init_end) (void);
	for (void (**pFunc) (void) = &__init_start; pFunc < &__init_end; pFunc++)
	{
		(**pFunc) ();
	}


  mmu_region page_mem = { .start_add = &_pages_start, .len = (&_pages_end - &_pages_start)};
  mmu_context mmu_ctx = mmu_init_tables(page_mem);
  mmu_enable(mmu_ctx);

  enable_icache_EL3();

  //write vbar_el3
  set_vbar_elx(3, &vbar_elx_vec);
  set_vbar_elx(2, &vbar_elx_vec);
  set_vbar_elx(1, &vbar_elx_vec);
  put64(SPLIT_ADDRESS(0x20),getVBAR());

  uart_init();
  timer_init();
  gpio_init();

  //call main after preinit
  boot_main();
}


/**************************************************/
/*                  Main                          */
/**************************************************/

int loop(void) {
  int i = 0;
  int j = 0;
  int cnt = 0;
  memset(text, 0, 256);

  
  // fast_trig_up();
  // wait_us(2);
  // invalidate_icache();
  //trig up

  
  for(i = 0;i<2; i++) {
    for(j = 0;j<2;j++) {
      cnt++;
    }
  }
  // fast_trig_down();

  //trig down
  // sprintf(text, "i=%d j=%d cnt=%d\n", i, j, cnt);
  // uart_print(text);

  return cnt;
}

int loop_trig(void) {
  int i = 0;
  int j = 0;
  int cnt = 0;
  memset(text, 0, 256);

  
  fast_trig_up();
  // wait_us(2);
  // invalidate_icache();
  //trig up

  
  for(i = 0;i<50; i++) {
    for(j = 0;j<50;j++) {
      cnt++;
    }
  }
  fast_trig_down();

  //trig down
  // sprintf(text, "i=%d j=%d cnt=%d\n", i, j, cnt);
  // uart_print(text);

  return cnt;
}

int boot_main ( void )
{  // uart_print("BOOTLOADER: Shall we play a game?\n\r");
  int cnt, res;
  int id = getCPUID();

  if(id == 0) {
    uart_print("Shall we play a game?\n");
  }


  // unsigned long el = getEL();


  while(1)
  {
    // invalidate_icache();
    int id = getCPUID();
    if(id == 0) {
      char cmd = uart_recv();
  
      switch(cmd) {
        case 't':
          uart_print("RPi3 Test OK\n");
          break;
        case 'm': // monitor other cores
          fast_trig_up();
          
          fast_trig_down();
          break;
        case 'r':
          put32(SHARED_ADDRESS(1*4), 0xFF);
          put32(SHARED_ADDRESS(2*4), 0xFF);
          put32(SHARED_ADDRESS(3*4), 0xFF);
          // DataSyncBarrier();
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 1));
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 2));
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 3));
          break;
        case 'l':
          //start loop on core 1
          fast_trig_up();
          // wait_us(1);
          // fast_trig_down();
          put32(SHARED_ADDRESS(1*4), 0xfe);
          put32(SHARED_ADDRESS(2*4), 0xfe);
          put32(SHARED_ADDRESS(3*4), 0xfe);
          // DataSyncBarrier();
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 1));
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 2));
          // clean_data_va((void*)SPLIT_ADDRESS_ID(0, 3));
          // fast_trig_up();
          // cnt = loop();
          // while ( get32(SHARED_ADDRESS(1*4)) > 8);
          //wait_us(5);
          int wait = 1000;
          while ( get32(SHARED_ADDRESS(1*4)) > 8 && wait-- > 0);
          cnt = get32(SHARED_ADDRESS(1*4));
          fast_trig_down();
          sprintf(text, "cnt=%d\n", cnt);
          uart_print(text);
          break;
        case 'v': // version
          uart_print("multi_core_loop.img\n");
          break;
        case '1':
          // inv_data_va((void*)SPLIT_ADDRESS_ID(4, 1));
          // DataSyncBarrier();
          res = get32(SHARED_ADDRESS(1*4));
          sprintf(text, "cnt1=%d\n", res);
          uart_print(text);
          break;
        case '2':
          // inv_data_va((void*)SPLIT_ADDRESS_ID(4, 2));
          // DataSyncBarrier();
          res = get32(SHARED_ADDRESS(2*4));
          sprintf(text, "cnt2=%d\n", res);
          uart_print(text);
          break;
        case '3':
          // inv_data_va((void*)SPLIT_ADDRESS_ID(4, 3));
          // DataSyncBarrier();
          res = get32(SHARED_ADDRESS(3*4));
          sprintf(text, "cnt3=%d\n", res);
          uart_print(text);
          break;
        default:
          //uart_print("Unknown command\n");
          //printf("Unknown command: %02x\n", cmd);
          break;
      }
    }
    else {
      // inv_data_va((void*)SPLIT_ADDRESS(0));
      // DataSyncBarrier();
      int cmd = get32(SHARED_ADDRESS(id*4));

      switch (cmd)
      {
        case 0xfe:
          // fast_trig_up();
          cnt = id;//loop() + (id << 16) ;
          put32(SHARED_ADDRESS(id*4), cnt);
          // fast_trig_down();
          // DataSyncBarrier();
          // clean_data_va((void*)SPLIT_ADDRESS(0));
          
          break;
        case 0xff://reset
          put32(SHARED_ADDRESS(id*4), 0);
          // DataSyncBarrier();
          // clean_data_va((void*)SPLIT_ADDRESS(0));
          break;
        default:
          break;
      }
    }
  }



  return 0;
}
