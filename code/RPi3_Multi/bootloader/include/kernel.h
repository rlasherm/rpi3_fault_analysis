/**
* @Author: ronan
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 01-09-2016
* @License: GPL
*/


#ifndef KERNEL_H
#define KERNEL_H

#define BOOT_METADATA_PTR 0x8000
#define TCR_EL3 0x80802020

extern unsigned int __bss_start__;
extern unsigned int __bss_end__;

extern unsigned int cores_activation ;


void boot_init ( void );
int boot_main ( void );
void boot_init_other ( void );
int boot_other ( void );
void main(void);
void initialise_monitor_handles(void);

#endif
