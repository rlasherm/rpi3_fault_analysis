/**
* @Author: Lashermes Ronan <ronan>
* @Date:   13-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 13-01-2017
* @License: MIT
*/



#ifndef INTERRUPT_HANDLER_H
#define INTERRUPT_HANDLER_H

void default_handler(void);

void synchronous_elx_sp0_handler(void);
void irq_elx_sp0_handler(void);
void fiq_elx_sp0_handler(void);
void serror_elx_sp0_handler(void);

void synchronous_elx_spx_handler(void);
void irq_elx_spx_handler(void);
void fiq_elx_spx_handler(void);
void serror_elx_spx_handler(void);

void other_core_interruption(unsigned int from);

#endif
