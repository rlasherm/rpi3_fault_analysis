/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 12-09-2016
* @License: GPL
*/

#ifndef UART_H
#define UART_H

#include "types.h"

#define GPPUD           0x3F200094
#define GPPUDCLK0       0x3F200098
#define AUX_ENABLES     0x3F215004
#define AUX_MU_IO_REG   0x3F215040
#define AUX_MU_IER_REG  0x3F215044
#define AUX_MU_IIR_REG  0x3F215048
#define AUX_MU_LCR_REG  0x3F21504C
#define AUX_MU_MCR_REG  0x3F215050
#define AUX_MU_LSR_REG  0x3F215054
#define AUX_MU_MSR_REG  0x3F215058
#define AUX_MU_SCRATCH  0x3F21505C
#define AUX_MU_CNTL_REG 0x3F215060
#define AUX_MU_STAT_REG 0x3F215064
#define AUX_MU_BAUD_REG 0x3F215068

//get state register (if data available or waiting to be sent...)
// unsigned int uart_get_reg ( void );
//read 1 byte of data
unsigned char uart_recv ( void );
//check if data available
// bool uart_available ( void );
//send 1 byte of data
void uart_send ( unsigned char c );
//flush input data
void uart_flush ( void );
//initialize uart
int uart_init ( void );
//print a string (prefer printf)
void uart_print(const char* str);
//print a number in hex format
void uart_printhex64( unsigned long d );
void uart_printhex32( unsigned int d );
void uart_printhex8( unsigned char d );
//read [len] bytes of data
void uart_read_vector(unsigned char* vec, unsigned int len);
//write [len] bytes of data
void uart_send_vector(unsigned char* vec, unsigned int len);
//print a byte vector in hex format
void uart_print_hexvector(unsigned char* vec, unsigned int len);

void uart_print_charhex ( unsigned char d );
//print a 32bit vector in hex format
void uart_print_32bvector(unsigned int* vec, unsigned int len);
void uart_print_msg ( char *msg );

void uart_beacon(void);
#endif
