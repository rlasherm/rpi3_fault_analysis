/**
 * @Author: kevin
 * @Date:   03-08-2017
 * @Email:  sebanjila.bukasa@inria.fr
 * @Last modified by:   kevin
 * @Last modified time: 03-08-2017
 */

 #ifndef FAULT_H
 #define FAULT_H

 // Mailboxes to set values

#define COREMBXSET 0x40000080
#define MBXSETC00  (COREMBXSET)
#define MBXSETC01  (COREMBXSET + 0x4)
#define MBXSETC02  (COREMBXSET + 0x8)
#define MBXSETC03  (COREMBXSET + 0xC)

#define MBXSETC10  (COREMBXSET + 0x10)
#define MBXSETC11  (COREMBXSET + 0x14)
#define MBXSETC12  (COREMBXSET + 0x18)
#define MBXSETC13  (COREMBXSET + 0x1C)

#define MBXSETC20  (COREMBXSET + 0x20)
#define MBXSETC21  (COREMBXSET + 0x24)
#define MBXSETC22  (COREMBXSET + 0x28)
#define MBXSETC23  (COREMBXSET + 0x2C)

#define MBXSETC30  (COREMBXSET + 0x30)
#define MBXSETC31  (COREMBXSET + 0x34)
#define MBXSETC32  (COREMBXSET + 0x38)
#define MBXSETC33  (COREMBXSET + 0x3C)

// Mailboxes to read or clear values

#define COREMBXRDCLR 0x400000C0
#define MBXRDCLRC00  (COREMBXRDCLR)
#define MBXRDCLRC01  (COREMBXRDCLR + 0x4)
#define MBXRDCLRC02  (COREMBXRDCLR + 0x8)
#define MBXRDCLRC03  (COREMBXRDCLR + 0xC)

#define MBXRDCLRC10  (COREMBXRDCLR + 0x10)
#define MBXRDCLRC11  (COREMBXRDCLR + 0x14)
#define MBXRDCLRC12  (COREMBXRDCLR + 0x18)
#define MBXRDCLRC13  (COREMBXRDCLR + 0x1C)

#define MBXRDCLRC20  (COREMBXRDCLR + 0x20)
#define MBXRDCLRC21  (COREMBXRDCLR + 0x24)
#define MBXRDCLRC22  (COREMBXRDCLR + 0x28)
#define MBXRDCLRC23  (COREMBXRDCLR + 0x2C)

#define MBXRDCLRC30  (COREMBXRDCLR + 0x30)
#define MBXRDCLRC31  (COREMBXRDCLR + 0x34)
#define MBXRDCLRC32  (COREMBXRDCLR + 0x38)
#define MBXRDCLRC33  (COREMBXRDCLR + 0x3C)

 #define SUMMARY  0x01
 #define TEST     0x02


 void boot_init_other_cores ( void );
 void main_fault_manager ( void );

 void main_wait_start( void );
 void unique_multi_core_aes (unsigned int core_id, unsigned int memory_space_size);
 void different_multi_core_aes (unsigned int core_id, unsigned int start_signal, unsigned int memory_space_size);
 void multi_aes_computation(unsigned char * plaintext,unsigned char * ciphertext,unsigned char * ciphertextverif,unsigned char * key, unsigned int ID, unsigned int memory_space_size);
 void set_privilege_level ( void );
 void goto_user_mode ( void );
 void is_alive ( void );
 void infinite_trigger ( void );
 long check_MMU_translation ( long address );
 int search_for_pattern ( void );
 unsigned int xorshift32(unsigned int state[static 1]);
 int infinite_load ( void );
 int infinite_store (void);

 #endif
