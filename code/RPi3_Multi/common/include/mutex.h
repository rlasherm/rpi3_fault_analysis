#ifndef MUTEX_H
#define MUTEX_H

#include <stdbool.h>

void lock_mutex(int* add);//blocking
void unlock_mutex(int* add);

#endif