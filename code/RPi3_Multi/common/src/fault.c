/**
 * @Author: kevin
 * @Date:   01-08-2017
 * @Email:  sebanjila.bukasa@inria.fr
 * @Last modified by:   kevin
 * @Last modified time: 03-08-2017
 */

#include "fault.h"
#include "gpio.h"
#include "uart.h"
#include "arm.h"
#include "mmu.h"
#include "kernel.h"
#include "myaes.h"
#include "timer.h"
#include "init.h"

void boot_init_other_cores ( void )
{

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int* bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  // initiate_jtag();
  // timer_init();
  // uart_init();
  // sink();

  // call construtors of static objects
	extern void (*__init_start) (void);
	extern void (*__init_end) (void);
	for (void (**pFunc) (void) = &__init_start; pFunc < &__init_end; pFunc++)
	{
		(**pFunc) ();
	}
  mmu_region page_mem = { .start_add = &_pages_start, .len = (&_pages_end - &_pages_start)};
  mmu_context mmu_ctx = mmu_init_tables(page_mem);
  mmu_enable(mmu_ctx);
  enable_icache_EL3();

  //enable_icache_EL3();
  //enable_dcache_EL3();
  //enable_others_mmu_EL3();

  //write vbar_el3
  set_vbar_elx(3, &vbar_other_cores);
  set_vbar_elx(2, &vbar_other_cores);
  set_vbar_elx(1, &vbar_other_cores);

  //call main after preinit
  //main_fault_manager();
  //main_wait_start();
  set_privilege_level();
}

/* Set privilege level of each cores */
void set_privilege_level ( void ){
  unsigned int cpuid;

  cpuid = getCPUID();

  switch(cpuid){
    case 0x01: // Monitor mode
    //main_wait_start();
    is_alive();
    break;
    case 0x02: // Kernel mode
    infinite_load();
    //infinite_trigger();
    //EL3toEL2((unsigned int) &(* main_wait_start));
    //branchto_arg();
    break;
    case 0x03: // User mode
    infinite_store();
    //is_alive2();
    //EL3toEL2((unsigned int) &(* goto_user_mode));
    //branchto_arg(bmeta_ptr->tos_entry, bmeta_ptr->tos_entry);
    break;
    default:
    break;
  }
  sink();
  return;
}

void goto_user_mode (void){
  EL2toEL1((unsigned int) &(* main_wait_start));
  return;
}

void main_fault_manager (void){
  // int ID2, ID = getCPUID(), dummy_value, nb_exec=0;
  // // sink();
  // unsigned char plaintext[16]={0x0,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
  // unsigned char ciphertext[16]={0};
  // unsigned char ciphertextverif[16]={0x69,0xC4,0xE0,0xD8,0x6A,0x7B,0x04,0x30,0xD8,0xCD,0xB7,0x80,0x70,0xB4,0xC5,0x5A};
  // unsigned char key[16]={0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xB,0xC,0xD,0xE,0xF};//{120,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};
  //
  // ID2 = ID-1;
  //
  // wait_us(100*ID2+20); //desynchronize access to memory regions
  //
  // put32((BASE_ADRESS_SHARE+0x4+(0x4*ID2)),ID);
  // put32(BASE_ADRESS_SHARE+0x20+(0x60*ID2),&vbar_other_cores);
  // put32(BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x4,&ciphertext);
  // put32(BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x8,&ciphertextverif);
  // while(1){
  //   AESEncrypt((ciphertext),(plaintext),key);
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0xC),get64(&key));
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x14),get64((unsigned int) &key + 0x8));
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x1C),get64(&plaintext));
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x24),get64((unsigned int) &plaintext + 0x8));
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x2C),get64(&ciphertext));
  //   put64((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x34),get64((unsigned int) &ciphertext + 0x8));
  //
  //   if(nb_exec == 0xFFFF){
  //     nb_exec = 1;
  //     invalidate_dcache(&plaintext);
  //     //invalidate_dcache(BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x4); // nb_exec
  //     //invalidate_dcache(BASE_ADRESS_SHARE+0x14+(0x4*ID2)); // is_alive
  //     //invalidate_dcache(BASE_ADRESS_SHARE+0x04+(0x4*ID2)); // is alive ++
  //   } else nb_exec++ ;
  //
  //   //put32((BASE_ADRESS_SHARE+0x20+(0x60*ID2)+0x4),nb_exec);
  //   put32((BASE_ADRESS_SHARE+0x14+(0x4*ID2)),nb_exec + ID2);
  //   DataSyncBarrier();
  //   if(nb_exec % 1000 == 0){
  //     wait_us(100*ID2+20); //desynchronize access to memory regions
  //     //put32((BASE_ADRESS_SHARE+0x14+(0x4*ID2)),0xFFFFFFFF || nb_exec || ID2);
  //   }
  //   for (dummy_value=0; dummy_value < 16; dummy_value++){
  //     if (strncmp(ciphertext[dummy_value],ciphertextverif[dummy_value],2) != 0){
  //       put32((BASE_ADRESS_SHARE+0x4+(0x4*ID2)),0xFFFFFFFF);
  //       sink();
  //     }
  //   }
  //   //invalidate_dcache(&plaintext); // load plaintext again
  // }
  sink();
}


/* Main of other cores when they are ready to work */
void main_wait_start(void){
  unsigned int start_signal=0;
  unsigned int trig_signal=0;
  unsigned int core_id=0;
  unsigned int exec = 0;
  unsigned int memory_space_size=MIN_SPACE;
  unsigned long registers_content;
  bool reg_selector = false;

  core_id = getCPUID(); //&&0xFF;
  put32(SHARED_ADRESS(0x4,0x10,core_id),getEL()); // check by writing EL
  //put32(SHARED_ADRESS(memory_space_size,0x0,core_id),getCBAR()); // check by writing EL
  registers_content=getVBAR();
  put64(SHARED_ADRESS(memory_space_size,0x0,core_id),registers_content);
  // load_data_reg_to_address(SHARED_ADRESS(memory_space_size,0x8,core_id));
  wait_us(1000*core_id+200); // desynchronize cores

  while(1){
    invalidate_dcache(BASE_ADRESS_SHARE);
    invalidate_dcache(BASE_ADRESS_SHARE+0x4); // size_reserved
    invalidate_dcache(BASE_ADRESS_SHARE+0x10);
    if (reg_selector == false ) {
      start_signal = get32(BASE_ADRESS_SHARE);
      trig_signal = start_signal >> 30;
      //start_signal &= 0x3FFFFFF8;
      //put32(SHARED_ADRESS(memory_space_size,0x0,core_id),0xCCCC0000|reg_selector);
      if (((start_signal >> core_id)&0x1) == 1){
        exec = NB_EXEC;
        // put32(BASE_ADRESS_SHARE+(reg_selector*0x10), (core_id << 0x1E));
        memory_space_size = get32(BASE_ADRESS_SHARE+0x4);
        reg_selector = true;
        //put32(SHARED_ADRESS(memory_space_size,0x0,core_id),0xDDDD0000|reg_selector);
      } // else // put32(BASE_ADRESS_SHARE+(reg_selector*0x10), (0x1 << core_id+16));
      //reg_selector = 1;
    } else if (reg_selector == true){
      start_signal = get32(BASE_ADRESS_SHARE+0x10);
      trig_signal = start_signal >> 30;
      //start_signal &= 0x3FFFFFF8;
      //put32(SHARED_ADRESS(memory_space_size,0x0,core_id),0xEEEE0000|reg_selector);
      if (((start_signal >> core_id)&0x1) == 1){
        exec = NB_EXEC;
        // put32(BASE_ADRESS_SHARE+(reg_selector*0x10), (core_id << 0x1E));
        memory_space_size = get32(BASE_ADRESS_SHARE+0x4);
        reg_selector = false;
        //put32(SHARED_ADRESS(memory_space_size,0x0,core_id),0xFFFF0000|reg_selector);
      } // else // put32(BASE_ADRESS_SHARE+(reg_selector*0x10), (0x1 << core_id+16));
      //reg_selector = 0;
    }
    while (exec > 0){
      if (memory_space_size > 0){
        if (CORE_ACTIVATED == 1){
          unique_multi_core_aes(core_id, memory_space_size);
        }
        else {
          different_multi_core_aes(core_id,trig_signal, memory_space_size);
        }
        exec--;
        //put32(SHARED_ADRESS(memory_space_size,0x3C,core_id),~exec|core_id);
        wait_us(10);
      } else {
        //put32(SHARED_ADRESS(0x4,0x10,core_id),0x55550000|core_id);
        exec = 0;
      }
    }
  }
  sink();
}

/* In case of only one other core launched */
void unique_multi_core_aes (unsigned int core_id, unsigned int memory_space_size){
  unsigned char plaintext[16]={0x0,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
  unsigned char ciphertext[16]={0};
  unsigned char ciphertextverif[16]={0x69,0xC4,0xE0,0xD8,0x6A,0x7B,0x04,0x30,0xD8,0xCD,0xB7,0x80,0x70,0xB4,0xC5,0x5A};
  unsigned char key[16]={0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xB,0xC,0xD,0xE,0xF};
  fast_trig_up();
  multi_aes_computation(plaintext,ciphertext,ciphertextverif,key,core_id,memory_space_size);
  fast_trig_down();
  return;
}

/* In case of several other core launched */
void different_multi_core_aes (unsigned int core_id, unsigned int start_signal, unsigned int memory_space_size){
  unsigned char plaintext[16]={0x0,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99,0xaa,0xbb,0xcc,0xdd,0xee,0xff};
  unsigned char ciphertext[16]={0};
  unsigned char ciphertextverif[16]={0x69,0xC4,0xE0,0xD8,0x6A,0x7B,0x04,0x30,0xD8,0xCD,0xB7,0x80,0x70,0xB4,0xC5,0x5A};
  unsigned char key[16]={0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xB,0xC,0xD,0xE,0xF};
  // if (((start_signal >> 0x2) && 0x3FFFFFFC) == core_id){
  if (start_signal == core_id){
    fast_trig_up();
    put32(SHARED_ADRESS(0x4,0x10,core_id),0x4C485300|core_id<<0x4);
  }
  multi_aes_computation(plaintext,ciphertext,ciphertextverif,key,core_id,memory_space_size);
  // if (((start_signal >> 0x2) && 0x3FFFFFFC) == core_id){
  if (start_signal == core_id){
    fast_trig_down();
    put32(SHARED_ADRESS(0x4,0x10,core_id),0x4C485300|core_id);
  }
  return;
}

/* Compute AES and store dummy values in shared memory location */
void multi_aes_computation(unsigned char * plaintext,unsigned char * ciphertext,unsigned char * ciphertextverif,unsigned char * key, unsigned int core_id, unsigned int memory_space_size){
  unsigned int i, mask=0;
  // put32(BASE_ADRESS_SHARE+0x20+(0x60*(core_id))+0x4,&ciphertext);
  // put32(BASE_ADRESS_SHARE+0x20+(0x60*(core_id))+0x8,&ciphertextverif);
  // put32(SHARED_ADRESS(memory_space_size,0x4,core_id), &ciphertext);
  // put32(SHARED_ADRESS(memory_space_size,0x8,core_id), &ciphertextverif);
  AESEncrypt(ciphertext,plaintext,key);
  wait_us(100);
  // put64(SHARED_ADRESS(memory_space_size,0xC,core_id), get64(key));
  // put64(SHARED_ADRESS(memory_space_size,0x14,core_id), get64((unsigned int) key + 0x8));
  // put64(SHARED_ADRESS(memory_space_size,0x1C,core_id), get64(plaintext));
  // put64(SHARED_ADRESS(memory_space_size,0x24,core_id), get64((unsigned int) plaintext + 0x8));
  // put64(SHARED_ADRESS(memory_space_size,0x2C,core_id), get64(ciphertext));
  // put64(SHARED_ADRESS(memory_space_size,0x34,core_id), get64((unsigned int) ciphertext + 0x8));
  // put32(SHARED_ADRESS(memory_space_size,0x3C,core_id), core_id&getCPUID());
  for (i=0; i < 15; i+=2){
    mask |= core_id<<(2*i);
  }
  for (i=0x0; i < MIN_SPACE ; i+=0x4){
    put32(SHARED_ADRESS(memory_space_size,i,core_id),mask);
  }
  return;
}

/* Activate GPIO ALT_TRIG_PIN, ALT_TRIG2_PIN or TRIG_PIN to set periodically a signal
 * Allow to check if a core is still alive
 */
void is_alive (void)
{
  int trigger_value = 500 ;
  long mmu_computed_value = 0 ;
  alt_gpio_init();
  for (int i=0x0; i < MIN_SPACE ; i+=0x8){
    mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
  }
  put64(SHARED_ADRESS(0x8,0xa0,0x1),(long) &trigger_value);
  put64(SHARED_ADRESS(0x8,0x80,0x1),mmu_computed_value);
  wait_us(500);
  while(1){
    wait_us(10000);
    fast_trig_up();
    wait_us(trigger_value);
    fast_trig_down();
    wait_us(trigger_value);
    for (int i=0x0; i < 0x200 ; i+=0x8){
      mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
      put64(SHARED_ADRESS(0x0,0x540+i,0x1),mmu_computed_value);
    }
    put64(SHARED_ADRESS(0x8,0x60,0x1),mmu_computed_value);
    //flush_TLB ();
  }
}

void infinite_trigger (void){
  int trigger_value = 1000;
  long mmu_computed_value = 0;
  for (int i=0x0; i < MIN_SPACE; i+=0x8){
    mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
  }
  put64(SHARED_ADRESS(0x8,0xa0,0x2),(long) &trigger_value);
  put64(SHARED_ADRESS(0x8,0x80,0x2),mmu_computed_value);
  while(1){
    wait_us(10000);
    fast_alt_trig_up();
    wait_us(trigger_value);
    fast_alt_trig_down();
    for (int i=0x0; i < 0x200 ; i+=0x8){
      mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
      put64(SHARED_ADRESS(0x0,0x320+i,0x1),mmu_computed_value);
    }
    put64(SHARED_ADRESS(0x8,0x60,0x2),mmu_computed_value);

    // uart_print("MMU computed: ");
    // uart_printhex64(mmu_computed_value); // 0xCC000000462980
    // uart_print("\n\r");
    // uart_print("@ Trigger val: ");
    // uart_printhex64((long) &trigger_value); // 0x462f6c
    // uart_print("\n\r");
    // uart_print("Trigger val: ");
    // uart_printhex64(trigger_value); // 0x3e8
    // uart_print("\n\r");
    // uart_print("\n\r");
  }
}

void is_alive2 (void)
{
  int trigger_value = 500 ;
  long mmu_computed_value = 0 ;
  for (int i=0x0; i < MIN_SPACE ; i+=0x8){
    mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
  }
  put64(SHARED_ADRESS(0x8,0xa0,0x3),(long) &trigger_value);
  put64(SHARED_ADRESS(0x8,0x80,0x3),mmu_computed_value);
  wait_us(1000);
  while(1){
    wait_us(10000);
    fast_alt_trig2_up();
    wait_us(trigger_value);
    fast_alt_trig2_down();
    wait_us(trigger_value);
    for (int i=0x0; i < 0x200 ; i+=0x8){
      mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
      put64(SHARED_ADRESS(0x0,0x100+i,0x1),mmu_computed_value);
    }
    put64(SHARED_ADRESS(0x8,0x60,0x3),mmu_computed_value);
    //flush_TLB ();
  }
}



/* Test MMU base address value
 * Perform a translation from a given adress
 * If VA = PA, address should be the same and return 0
 * If not return computed_value
*/
long check_MMU_translation ( long address ){
  long expected_value, computed_value ;
  expected_value = address ;
  MMU_translate( address );
  computed_value = get_address_translate() ;
  if(expected_value == computed_value){
    return 0xFFFFFFFFFFFFFFFF;
  }
  else return computed_value ;

}

/* Test
  Executer une application non signée
  On doit charger l'application dans le cache
*/
int infinite_load ( void ){
  uint32_t state = 777747, x, x1, x2, state2 = 767757;
  wait_us(1000);
  while(1){
    fast_alt_trig_up();
    wait_us(1000);
    x1 = xorshift32(state);
    x2 = xorshift32(state2);
    state = 0x00000000FFFFFFFF & (x1 | x2) ;
    x =  state;
    put64(SHARED_ADRESS(0x8,0x60,0x2),(long) &state);
    put64(SHARED_ADRESS(0x8,0x80,0x2),state);
    put64(SHARED_ADRESS(0x8,0xa0,0x2),x);
    // load_data_reg_to_address(SHARED_ADRESS(0x0,0x100,0x2));
    for (int i=0x0; i < 0x2000 ; i+=0x8){
      if (x+i < 0x0FFFFFFF){
      get32(0x00000000FFFFFFFF&(x+i));
      }
        // mmu_computed_value = check_MMU_translation ( (long) &trigger_value + i);
        // put64(SHARED_ADRESS(0x0,0x100+i,0x1),mmu_computed_value);
      }
    state2 = x2;
    put64(SHARED_ADRESS(0x8,0xc0,0x2),state2);
    fast_alt_trig_down();
    wait_us(500);
  }
  return 0;
}

int infinite_store ( void ){
  uint32_t state = 380350, x, x1, x2, state2 = 320330;
  long value;
  wait_us(1000);
  while(1){
    fast_alt_trig2_up();
    wait_us(1000);
    x1 = xorshift32(state);
    x2 = xorshift32(state2);
    state = 0x00000000FFFFFFFF & (x1 | x2) ;
    x =  state ;
    put64(SHARED_ADRESS(0x8,0x60,0x3),(long) &state);
    put64(SHARED_ADRESS(0x8,0x80,0x3),state);
    put64(SHARED_ADRESS(0x8,0xa0,0x3),x);
    // load_data_reg_to_address(SHARED_ADRESS(0x60,0x100,0x3));
    for (int i=0x0; i < 0x2000 ; i+=0x8){
      if (x+i < 0x0FFFFFFF){
      value = get32(0x00000000FFFFFFFF&(x+i));
      put32(0x00000000FFFFFFFF&(x+i),value);
      }
        // mmu_computed_value = check_MMU_translation((long) &trigger_value + i);
        // put64(SHARED_ADRESS(0x0,0x100+i,0x1),mmu_computed_value);
      }
    state2 = x2;
    put64(SHARED_ADRESS(0x8,0xc0,0x3),state2);
    fast_alt_trig2_down();
    wait_us(500);
  }
  return 0;
}

/* pseudo_random_generator */
unsigned int xorshift32(unsigned int state[static 1])
{
	unsigned int x = state[0];
	x ^= x << 13;
	x ^= x >> 17;
	x ^= x << 5;
	state[0] = x;
	return x;
}
