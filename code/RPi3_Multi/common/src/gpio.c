/**
* @Author: ronan
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/
#include "gpio.h"
#include "arm.h"
#include "timer.h"
#include "mem.h"

//base for gpios
volatile unsigned int* gpio = (unsigned int*)GPIO_BASE;

//There are 53 gpios split over 2 32bits registers. Compute the offset 0 or 1 according to wich register it is in.
unsigned int pin_reg_offset(unsigned int pin_select) {
  if(pin_select <= 53) {
    return (pin_select/32);
  }
  else {
    return 0;
  }
}

//Return the position of the bit to modify in the register specific to this pin
unsigned int pin_bit_offset(unsigned int pin_select) {
  if(pin_select <= 53) {
    return (pin_select%32);
  }
  else {
    return 0;
  }
}

//Compute the function select reg offset (0 to 5) for this pin
unsigned int fsel_reg_offset(unsigned int pin_select) {
  if(pin_select <= 53) {
    return (pin_select/10);
  }
  else {
    return 0;
  }
}

//Compute the function select bit offset (0 to 5) for this pin
unsigned int fsel_bit_offset(unsigned int pin_select) {
  if(pin_select <= 53) {
    return (pin_select%10)*3;
  }
  else {
    return 0;
  }
}

bool set_pin_function(unsigned int pin_select, unsigned int type) {
  if(pin_select <= 53) {
    type = type & 7;//fun sel on 3 bits
    unsigned int prev = gpio[GPIO_GPFSEL0+fsel_reg_offset(pin_select)];
    // unsigned int prev = get32(GPIO_BASE+GPIO_GPFSEL0+fsel_reg_offset(pin_select));
    prev &= ~(7<< fsel_bit_offset(pin_select));
    prev |= type<< fsel_bit_offset(pin_select);
    gpio[GPIO_GPFSEL0+fsel_reg_offset(pin_select)] = prev;
    // put32(GPIO_BASE+GPIO_GPFSEL0+fsel_reg_offset(pin_select), prev);
    return true;
  }
  else {
    return false;
  }

}

bool write_pin(unsigned int pin_select, bool value) {
  if(pin_select <= 53) {
    unsigned int write_to = 0;
    if(value == true) {
      write_to = GPIO_GPSET0;
    }
    else {
      write_to = GPIO_GPCLR0;
    }
    gpio[write_to+pin_reg_offset(pin_select)] = (1 << pin_bit_offset(pin_select));
    // put32(GPIO_BASE+write_to+pin_reg_offset(pin_select), (1 << pin_bit_offset(pin_select)));
    return true;
  }
  else {
    return false;
  }
}

bool read_pin(unsigned int pin_select) {
  if(pin_select <= 53) {
    // return (get32(GPIO_BASE+GPIO_GPLEV0+pin_reg_offset(pin_select)) & pin_bit_offset(pin_select)) != 0;
    return (gpio[GPIO_GPLEV0+pin_reg_offset(pin_select)] & pin_bit_offset(pin_select)) != 0;
  }
  else {
    return false;
  }
}

void toggle_pin(unsigned int pin_select) {
  write_pin(pin_select, !read_pin(pin_select));
}

void turn_on_LED(void) {
}

void turn_off_LED(void) {
}

void toggle_LED(void) {
}

//pin 16
void fast_trig_up(void) {
  put32(GPSET0, 0x10000);
}
void fast_trig_down(void) {
  put32(GPCLR0, 0x10000);
}
// pin 17
void fast_alt_trig_up (void){
  put32(GPSET0, 0x20000);
}

void fast_alt_trig_down (void){
  put32(GPCLR0, 0x20000);
}
// pin 26
void fast_alt_trig2_up (void){
  put32(GPSET0, 0x4000000);
}

void fast_alt_trig2_down (void){
  put32(GPCLR0, 0x4000000);
}

void error_code_LED(unsigned int error) {
  unsigned int i = 0;
  while(1) {
    for(i=0; i< error; i++) {
      turn_on_LED();
      wait_us(200000);
      turn_off_LED();
      wait_us(250000);
    }
    wait_us(1000000);
  }
}

void alt_gpio_init(void) {
  //set trig pin as output
  set_pin_function(ALT_TRIG_PIN, FSEL_OUTPUT);
  set_pin_function(ALT_TRIG_PIN2, FSEL_OUTPUT);
}
