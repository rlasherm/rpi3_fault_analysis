/**
* @Author: Lashermes Ronan <ronan>
* @Date:   06-02-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 06-02-2017
* @License: MIT
*/
#include "mmu.h"
#include "arm.h"
#include "types.h"
#include "mem.h"

//set the enable_level flags from T0SZ value
int enabled_first_level_from_T0SZ(int T0SZ)
{
  if(T0SZ <= 24 && T0SZ >= 16)
  {
    return 0;
  }
  else if(T0SZ <= 33 && T0SZ >= 25)
  {
    return 1;
  }
  else if(T0SZ <= 39 && T0SZ >= 34)
  {
    return 2;
  }
  else
  {
    //invalid T0SZ;
    return -1;
  }
}

// ************************** PAGE MEM ALLOCATION ****************************

//alignement in bytes
//align 16 => returned address must be 0xXXXX0
//cursor is moved to aligned value (same as returned)
void* align_cursor(size_t alignement, void** cursor, void* end_mem)
{
  if(*cursor == NULL || end_mem == NULL)
  {
    return NULL;//propagate NULL
  }

  if((unsigned long)(*cursor) % alignement == 0)//already aligned
  {
    if(*cursor > end_mem)
    {
      return NULL;
    }
    else
    {
      return *cursor;
    }
  }
  else
  {
    void* new_val = (void *)(alignement - ((unsigned long)(*cursor) % alignement) + (unsigned long)(*cursor));
    if(new_val > end_mem)
    {
      return NULL;
    }
    else
    {
      *cursor = new_val;
      return new_val;
    }
  }
}

//cursor is moved, starting address is returned
void* reserve_space(size_t byte_space, void** cursor, void* end_mem)
{
  if(*cursor == NULL || end_mem == NULL)
  {
    return NULL;//propagate NULL
  }

  void* new_val = *cursor + byte_space;
  if(new_val > end_mem)
  {
    return NULL;
  }
  else
  {
    void* ret = *cursor;
    *cursor = new_val;
    return ret;
  }
}

//reserve some space in memory (of len byte_space, starting from cursor).
//return starting address of the reserved space (fullfilling alignement) if valid
//return NULL if end of memory (end_mem) is reaches
void* reserve_aligned_space(size_t byte_space, size_t alignement, void** cursor, void* end_mem)
{
  //NULL is propagated, so no need to check
  align_cursor(alignement, cursor, end_mem);
  return reserve_space(byte_space, cursor, end_mem);
}

// ***************************************************************************

unsigned long createMAIR()
{
  unsigned long mair =  /*0x88*/ /*0x77 */ 0xCC /*0x44*/ << (ATTRINDX_NORMAL*8)	// inner/outer write-back non-transient, non-allocating
	                | 0x04 << (ATTRINDX_DEVICE*8)	// Device-nGnRE
	                // | 0x00 << (ATTRINDX_COHERENT*8);	// Device-nGnRnE
                  | 0x88 << (ATTRINDX_COHERENT*8);

  return mair;
}

static void* page_cursor = 0;
static void* page_end = 0;
void* palloc(size_t page_size)
{
  return reserve_aligned_space(page_size, page_size, &page_cursor, page_end);
}

TARMV8MMU_LEVEL2_DESCRIPTOR *createLevel2Table (void)
{
  TARMV8MMU_LEVEL2_DESCRIPTOR *l2_pTable = (TARMV8MMU_LEVEL2_DESCRIPTOR *) palloc (ARMV8MMU_GRANULE_SIZE);

  mo_memset (l2_pTable, 0, PAGE_SIZE);

  for (unsigned nEntry = 0; nEntry < 3; nEntry++)		// 3 entries a 512MB
  {
    u64 nBaseAddress = (u64) nEntry * ARMV8MMU_TABLE_ENTRIES * ARMV8MMU_LEVEL3_PAGE_SIZE;

    TARMV8MMU_LEVEL3_DESCRIPTOR *pTable = createLevel3Table (nBaseAddress);

    TARMV8MMU_LEVEL2_TABLE_DESCRIPTOR *pDesc = &l2_pTable[nEntry].Table;

    pDesc->Value11	    = 3;
    pDesc->Ignored1	    = 0;
    pDesc->TableAddress = ARMV8MMUL2TABLEADDR ((u64) pTable);
    pDesc->Reserved0    = 0;
    pDesc->Ignored2	    = 0;
    pDesc->PXNTable	    = 0;
    pDesc->UXNTable	    = 0;
    pDesc->APTable	    = AP_TABLE_ALL_ACCESS;
    pDesc->NSTable	    = 0;
  }

  DataSyncBarrier ();
  return l2_pTable;
}

TARMV8MMU_LEVEL3_DESCRIPTOR *createLevel3Table (u64 nBaseAddress)
{
	TARMV8MMU_LEVEL3_DESCRIPTOR *pTable = (TARMV8MMU_LEVEL3_DESCRIPTOR *) palloc (ARMV8MMU_LEVEL3_PAGE_SIZE);
	// assert (pTable != 0);

	for (unsigned nPage = 0; nPage < ARMV8MMU_TABLE_ENTRIES; nPage++)	// 8192 entries a 64KB
	{
		TARMV8MMU_LEVEL3_PAGE_DESCRIPTOR *pDesc = &pTable[nPage].Page;

		pDesc->Value11	     = 3;
		pDesc->AttrIndx	     = ATTRINDX_NORMAL;
		pDesc->NS	     = 0;
		pDesc->AP	     = ATTRIB_AP_RW_EL1;
    	// pDesc->AP	     = ATTRIB_AP_RW_ALL;
		pDesc->SH	     = ATTRIB_SH_INNER_SHAREABLE;
		pDesc->AF	     = 1;
		pDesc->nG	     = 0;
		pDesc->Reserved0_1   = 0;
		pDesc->OutputAddress = ARMV8MMUL3PAGEADDR (nBaseAddress);
		pDesc->Reserved0_2   = 0;
		pDesc->Continous     = 0;
		pDesc->PXN	     = 0;
		// pDesc->UXN	     = 1;
    pDesc->UXN	     = 0;
		pDesc->Ignored	     = 0;


    if (nBaseAddress >= MEM_DEVICE_START)
    {
    	pDesc->AttrIndx = ATTRINDX_DEVICE;
    	pDesc->SH	= ATTRIB_SH_OUTER_SHAREABLE;
    }
    // else if(BASE_ADDRESS_SHARE <= nBaseAddress && nBaseAddress < END_ADDRESS_SPLIT) 
    // {
    //   pDesc->AttrIndx = ATTRINDX_DEVICE;
    // 	pDesc->SH	= ATTRIB_SH_OUTER_SHAREABLE;
    // }
    // else
    // {
    //   pDesc->AttrIndx	= ATTRINDX_NORMAL;
    // 	// pDesc->AttrIndx = ATTRINDX_COHERENT;
    // 	pDesc->SH	= ATTRIB_SH_OUTER_SHAREABLE;
    //   // pDesc->SH	= ATTRIB_SH_INNER_SHAREABLE;
    // }

		nBaseAddress += ARMV8MMU_LEVEL3_PAGE_SIZE;
	}

	return pTable;
}

mmu_context mmu_id_test(void)
{
  mmu_context context;
  context.ttbr = (void*)createLevel2Table();
  return context;
}

//pages_res is the memory region reserved for pages
//if the region is too small, the method return a NULL pointer.
mmu_context mmu_init_tables(mmu_region pages_res)
{
  page_cursor = pages_res.start_add;
  page_end = pages_res.start_add + pages_res.len;

  // int T0SZ = 64 - LOG2_IA;
  //get enabled levels according to T0SZ (p. D4-1764)
  // int first_level = enabled_first_level_from_T0SZ(T0SZ);



  //create identity mapping
  mmu_context context =  mmu_id_test();
  context.mair = createMAIR();
  u64 nTCR_EL1 = getTCR_EL3();
	nTCR_EL1 &= ~(  TCR_EL1_IPS__MASK
		      | TCR_EL1_A1
		      | TCR_EL1_TG0__MASK
		      | TCR_EL1_SH0__MASK
		      | TCR_EL1_ORGN0__MASK
		      | TCR_EL1_IRGN0__MASK
		      | TCR_EL1_EPD0
		      | TCR_EL1_T0SZ__MASK);
	nTCR_EL1 |=   TCR_EL1_EPD1
		    | TCR_EL1_IPS_4GB	    << TCR_EL1_IPS__SHIFT
		    | TCR_EL1_TG0_64KB	    << TCR_EL1_TG0__SHIFT
		    | TCR_EL1_SH0_INNER     << TCR_EL1_SH0__SHIFT
		    | TCR_EL1_ORGN0_WR_BACK << TCR_EL1_ORGN0__SHIFT
		    | TCR_EL1_IRGN0_WR_BACK << TCR_EL1_IRGN0__SHIFT
		    | TCR_EL1_T0SZ_4GB	    << TCR_EL1_T0SZ__SHIFT;
  context.tcr = nTCR_EL1;

  // setMAIR_EL3(createMAIR());

  // uart_print("ttbr: 0x");
  // uart_printhex64((u64)context.ttbr);
  // uart_print("\n\r");

  return context;
}

void mmu_enable(mmu_context context)
{
  setMAIR_EL3(context.mair);
  enableMMU_EL3(context.ttbr, context.tcr);
}
