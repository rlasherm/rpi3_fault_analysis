/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 03-10-2016
* @License: GPL
*/

#include "arm.h"
#include "uart.h"
#include "mem.h"
#include "gpio.h"
#include "mutex.h"
#include <string.h>

//get state register (if data available or waiting to be sent...)
// unsigned int uart_get_reg ( void )
// {
//     return (get32(AUX_MU_LSR_REG));
// }
//read 1 byte of data
unsigned char uart_recv ( void )
{
//   lock_mutex((void *) UART_LOCK);
  while(1)
  {
      if(get32(AUX_MU_LSR_REG) & 0x01) break;
  }
//   unlock_mutex((void *) UART_LOCK);
  return (unsigned char)(get32(AUX_MU_IO_REG)&0xFF);
}
// check if data available
// bool uart_available ( void )
// {
//     return (get32(AUX_MU_LSR_REG) & 0x01) != 0;
// }
//send 1 byte of data
void uart_send ( unsigned char c )
{
//   lock_mutex((void *) UART_LOCK);
  while(1)
  {
      if(get32(AUX_MU_LSR_REG) & 0x20) break;
  }
  put32(AUX_MU_IO_REG,(unsigned int)c);
//   unlock_mutex((void *) UART_LOCK);
}
//print a string (prefer printf)
void uart_print(const char* str) {
    // lock_mutex((void *) UART_LOCK);
    int i;
    int len = strlen(str);
    // int len = strcount(str);
    for(i=0; i<len;i++) {
        uart_send((unsigned int)str[i]);
    }
    // unlock_mutex((void *) UART_LOCK);
}
//flush input data
void uart_flush ( void )
{
    // lock_mutex((void *) UART_LOCK);
    while(1)
    {
        if((get32(AUX_MU_LSR_REG)&0x100)==0) break;
    }
    // unlock_mutex((void *) UART_LOCK);
}

void uart_beacon(void)
{
    // lock_mutex((void *) UART_LOCK);
    while(1)
    {
        if(get32(AUX_MU_LSR_REG) & 0x20) break;
    }
    put32(AUX_MU_IO_REG,(unsigned int)'!');
    // unlock_mutex((void *) UART_LOCK);
}

//print a u32 in hex format
void uart_printhex8( unsigned char d )
{
    // lock_mutex((void *) UART_LOCK);
    unsigned int rb;
    unsigned int rc;

    rb=8;
    while(1)
    {
        rb-=4;
        rc=(d>>rb)&0xF;
        if(rc>9) rc+=0x37; else rc+=0x30;
        uart_send(rc);
        if(rb==0) break;
    }
    // unlock_mutex((void *) UART_LOCK);
}

//print a u32 in hex format
void uart_printhex64( unsigned long d )
{
    // lock_mutex((void *) UART_LOCK);
    unsigned int rb;
    unsigned int rc;

    rb=64;
    while(1)
    {
        rb-=4;
        rc=(d>>rb)&0xF;
        if(rc>9) rc+=0x37; else rc+=0x30;
        uart_send(rc);
        if(rb==0) break;
    }
    // unlock_mutex((void *) UART_LOCK);
}

void uart_printhex32( unsigned int d )
{
    // lock_mutex((void *) UART_LOCK);
    unsigned int rb;
    unsigned int rc;

    rb=32;
    while(1)
    {
        rb-=4;
        rc=(d>>rb)&0xF;
        if(rc>9) rc+=0x37; else rc+=0x30;
        uart_send(rc);
        if(rb==0) break;
    }
    // unlock_mutex((void *) UART_LOCK);
}

void uart_print_charhex ( unsigned char d )
{
    // uart_send('0');
    // uart_send('x');
    //unsigned int ra;
    unsigned int rb;
    unsigned int rc;

    rb=8;
    while(1)
    {
        rb-=4;
        rc=(d>>rb)&0xF;
        if(rc>9) rc+=0x37; else rc+=0x30;
        uart_send(rc);
        if(rb==0) break;
    }
}

//initialize uart
int uart_init ( void )
{
    // lock_mutex((void *) UART_LOCK);
    unsigned int ra;

    put32(AUX_ENABLES,1);
    put32(AUX_MU_IER_REG,0);
    put32(AUX_MU_CNTL_REG,0);
    put32(AUX_MU_LCR_REG,3);
    put32(AUX_MU_MCR_REG,0);
    put32(AUX_MU_IER_REG,0);
    put32(AUX_MU_IIR_REG,0xC6);
    put32(AUX_MU_BAUD_REG,270);

    set_pin_function(14, FSEL_ALT5);
    set_pin_function(15, FSEL_ALT5);

    put32(GPPUD,0);
    for(ra=0;ra<150;ra++) dummy();
    put32(GPPUDCLK0,(1<<14)|(1<<15));
    for(ra=0;ra<150;ra++) dummy();
    put32(GPPUDCLK0,0);

    put32(AUX_MU_CNTL_REG,3);
    // unlock_mutex((void *) UART_LOCK);
    return(0);
}

//read [len] bytes of data
void uart_read_vector(unsigned char* vec, unsigned int len) {
    // lock_mutex((void *) UART_LOCK);
    unsigned int i;
    for(i=0; i < len; i++) {
        vec[i] = uart_recv();
    }
    // unlock_mutex((void *) UART_LOCK);
}

//write [len] bytes of data
void uart_send_vector(unsigned char* vec, unsigned int len) {
    // lock_mutex((void *) UART_LOCK);
    unsigned int i;
    for(i=0; i < len; i++) {
        uart_send(vec[i]);
    }
    // unlock_mutex((void *) UART_LOCK);
}

//print a byte vector in hex format
void uart_print_hexvector(unsigned char* vec, unsigned int len) {
    // lock_mutex((void *) UART_LOCK);
    unsigned int i;
    for(i=0; i < len; i++) {
        uart_print_charhex(vec[i]);
    }
    // unlock_mutex((void *) UART_LOCK);
}

//print a byte vector in hex format
void uart_print_32bvector(unsigned int* vec, unsigned int len) {
  unsigned int i;
  for(i=0; i < len; i++) {
    uart_printhex32(vec[i]);
    uart_send(0x7c);
  }
}

// send a message
void uart_print_msg (char *msg)
{
    // lock_mutex((void *) UART_LOCK);
    int i = 0;
    while(msg[i])
        {
        uart_send(msg[i]);
        i++;
        }
    uart_send(0x0D);
    uart_send(0x0A);
    // unlock_mutex((void *) UART_LOCK);
}
