/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 02-09-2016
* @License: GPL
*/

#include "timer.h"
#include "arm.h"
#include "mem.h"

void timer_init(void) {
  // put32(TIMER_BASE,0x00000000);
  // put32(TIMER_BASE,0x00000200);
  put32(TIMER_BASE,0x00F90000);
  put32(TIMER_BASE,0x00F90200);
}

unsigned int get_timer_tick(void) {
  return get32(TIMER_LO);
}

unsigned long get_timer_tick64(void) {
  unsigned long lo = (unsigned long)get32(TIMER_LO);
  unsigned long hi = (unsigned long)get32(TIMER_HI);
  return (hi << 32l) | lo;
}

// void wait_us(unsigned long us) {
//   unsigned long tick_start = get_timer_tick64();
//   while(get_timer_tick64() - tick_start < us) {
//     // dummy();
//     asm ("nop");
//   }
// }

// void wait_us(unsigned long us) {
//   volatile unsigned long *timer = (unsigned long *)TIMER_LO;
//   unsigned long tick_start = *timer;//get_timer_tick64();
//   while(*timer - tick_start < us) {
//     asm ("nop");
//   }
// }

void wait_us(unsigned long us) {
  int tot = 100*us, i;
  for(i = 0; i< tot; i++) {
    asm ("nop");
  }
}
