# @Author: Lashermes Ronan <ronan>
# @Date:   13-01-2017
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 13-01-2017
# @License: MIT



export COMMON_DEPS = arm.h gpio.h init.h interrupt_handler.h mem.h timer.h types.h uart.h jtag.h \
							 \

export COMMON_OBJ = 	arm.o gpio.o init.o interrupt_handler.o mem.o timer.o \
 							uart.o jtag.o vector_table.o cstub.o mmu.o myaes.o  \
