<!--
@Author: Lashermes Ronan <ronan>
@Date:   20-01-2017
@Email:  ronan.lashermes@inria.fr
@Last modified by:   ronan
@Last modified time: 20-01-2017
@License: MIT
-->



# Exception levels (EL)

There are 4 exception levels (el) in ARMv8:
- EL0 (user mode)
- EL1 (kernel mode/supervisor)
- EL2 (hypervisor)
- EL3 (monitor, to switch between secure and non-secure modes)

## Switching between levels

We boot in Secure EL3, allowing to do a proper setup before going to less priviledged levels.

### EL3 to EL2

In input, x0 contains the address where we will start in EL2.
```
mov	x1, #0x5b1	/* Non-secure EL0/EL1 | HVC | 64bit EL2 */
msr	scr_el3, x1
msr	cptr_el3, xzr	/* Disable coprocessor traps to EL3 */
mov	x1, #0x33ff
msr	cptr_el2, x1	/* Disable coprocessor traps to EL2 */

/* Initialize SCTLR_EL2 */
msr	sctlr_el2, xzr

/* Return to the EL2_SP2 mode from EL3 */
mov	x1, sp
msr	sp_el2, x1	/* Migrate SP */
mrs	x1, vbar_el3
msr	vbar_el2, x1	/* Migrate VBAR */
mov	x1, #0x3c9
msr	spsr_el3, x1	/* EL2_SP2 | D | A | I | F */
msr	elr_el3, x0
eret
```

### EL2 to EL1

In input, x0 contains the address where we will start in EL1.
```
/* Initialize Generic Timers */
mrs	x2, cnthctl_el2
orr	x2, x2, #0x3		/* Enable EL1 access to timers */
msr	cnthctl_el2, x2
msr	cntvoff_el2, xzr
mrs	x2, cntkctl_el1
orr	x2, x2, #0x3		/* Enable EL0 access to timers */
msr	cntkctl_el1, x2

/* Initilize MPID/MPIDR registers */
mrs	x2, midr_el1
mrs	x1, mpidr_el1
msr	vpidr_el2, x2
msr	vmpidr_el2, x1

/* Disable coprocessor traps */
mov	x2, #0x33ff
msr	cptr_el2, x2		/* Disable coprocessor traps to EL2 */
msr	hstr_el2, xzr		/* Disable coprocessor traps to EL2 */
mov	x2, #3 << 20
msr	cpacr_el1, x2		/* Enable FP/SIMD at EL1 */

/* Initialize HCR_EL2 */
mov	x2, #(1 << 31)		/* 64bit EL1 */
orr	x2, x2, #(1 << 29)	/* Disable HVC */
msr	hcr_el2, x2

/* SCTLR_EL1 initialization */
mov	x2, #0x0800
movk	x2, #0x30d0, lsl #16
msr	sctlr_el1, x2

/* Return to the EL1_SP1 mode from EL2 */
mov	x2, sp
msr	sp_el1, x2		/* Migrate SP */
mrs	x2, vbar_el2
msr	vbar_el1, x2		/* Migrate VBAR */
mov	x2, #0x3c5
msr	spsr_el2, x2		/* EL1_SP1 | D | A | I | F */
msr	elr_el2, x0
eret
```
