# JTAG

JTAG is the protocol used for debugging. Here we will use it.
The sofware part is an openocd + gdb combo. The hardware jtag is a BusBlaster V4.1.


## RPi3 pins

Cf [pinout](http://pinout.xyz).
The jtag pins have to be enabled on the RPi3 for jtag to be available.

```C
set_pin_function(4, FSEL_ALT5);
set_pin_function(22, FSEL_ALT4);
set_pin_function(24, FSEL_ALT4);
set_pin_function(25, FSEL_ALT4);
set_pin_function(27, FSEL_ALT4);
```

The pins have the following jtag labeling:
|---|---|
|Rpi pin|Jtag label|
|---|---|
|4|TDI|
|22|TRST|
|24|TDO|
|25|TCK|
|27|TMS|

Do not forget to connect the ground (GND).

## BusBlaster

Connect the BusBlaster to the RPi3, do connect the grounds but not the vcc/vtg.

## openocd

Follow the guidelines at [rpi3-aarch64-jtag](https://github.com/daniel-k/rpi3-aarch64-jtag).
Build your own openocd.

Use the **./connect.sh** script to launch openocd.

## gdb

You must use the gdb from the adequate toolchain.
In the toolchains/aarch64/bin folder.
```
./aarch64-elf-gdb
```
