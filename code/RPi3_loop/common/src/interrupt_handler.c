/**
* @Author: Lashermes Ronan <ronan>
* @Date:   13-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 13-01-2017
* @License: MIT
*/
#include "interrupt_handler.h"
#include "uart.h"
#include "arm.h"
#include <stdio.h>

void uart_printcrashreport_el3()
{
  // printf("Crash @ 0x%lx\n\r", (unsigned long)getELR_EL3());
  // printf("May involve memory access to 0x%lx\n\r", (unsigned long)getFAR_EL3());
  uart_print("\t- Crash @ 0x");
  uart_printhex64((unsigned long)getELR_EL3());
  uart_print("\n\r\t- May involve memory access to 0x");
  uart_printhex64((unsigned long)getFAR_EL3());
  uart_print("\n\r\t- Cause is ESR = 0x");
  uart_printhex32(getESR_EL3());
  uart_print("\n\r");
}

void uart_printcrashreport()
{
  uart_print("Crash! Report:\n\r");
  int el = getEL();
  // printf("Secure state: EL%d\n\r", el);
  uart_print("\t- Secure state: EL");
  uart_printhex8((unsigned char)el);
  uart_print("\n\r");

  switch(el)
  {
    case 3:
      uart_printcrashreport_el3();
      break;
  }
  while(1) {}
}

void default_handler(void)
{
  uart_print("default handler\n\r");
}

void synchronous_elx_sp0_handler(void)
{
  uart_print("synchronous_elx_sp0_handler\n\r");
  uart_printcrashreport();
}

void irq_elx_sp0_handler(void)
{
  uart_print("irq_elx_sp0_handler\n\r");
  uart_printcrashreport();
}

void fiq_elx_sp0_handler(void)
{
  uart_print("fiq_elx_sp0_handler\n\r");
  uart_printcrashreport();
}

void serror_elx_sp0_handler(void)
{
  uart_print("serror_elx_sp0_handler\n\r");
  uart_printcrashreport();
}

void synchronous_elx_spx_handler(void)
{
  uart_print("synchronous_elx_spx_handler\n\r");
  uart_printcrashreport();
}

void irq_elx_spx_handler(void)
{
  uart_print("irq_elx_spx_handler\n\r");
}

void fiq_elx_spx_handler(void)
{
  uart_print("fiq_elx_spx_handler\n\r");
}

void serror_elx_spx_handler(void)
{
  uart_print("serror_elx_spx_handler\n\r");
  while(1) {}
}
