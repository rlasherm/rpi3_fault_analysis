/**
* @Author: kevin, ronan
* @Date:   31-08-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 31-08-2016
* @License: GPL
*/



#include "mem.h"
#include <stdlib.h>
#include <string.h>

//String compare
// int strcmp(const char *s1, const char *s2, size_t n)
// {
//      if (n == 0)
//          return (0);
//      do {
//          if (*s1 != *s2++)
//              return (*(unsigned char *) s1 - *(unsigned char *) --s2);
//          if (*s1++ == 0)
//              break;
//      } while (--n != 0);
//      return (0);
// }
//
// // /*
// //  * Copy @len bytes from @src to @dst
// //  */
// void *memcpy(void *dst, const void *src, size_t len)
// {
//   const char *s = src;
//   char *d = dst;
//   while (len--)
//     *d++ = *s++;
//   return dst;
// }
//
// int memcmp(const void *s1, const void *s2, size_t n)
// {
//   const unsigned char *p1 = s1, *p2 = s2;
//   while(n--)
//       if( *p1 != *p2 )
//           return *p1 - *p2;
//       else
//           p1++,p2++;
//   return 0;
// }

void *mo_memset(void *s, int c, size_t n)
{
  unsigned char* p=s;
   while(n--)
       *p++ = (unsigned char)c;
   return s;
}


// int strcount(const char* str)
// {
//   int i, max = 1024;
//   for(i=0; i < max; i++)
//   {
//     if(str[i] == '\0')
//     {
//       return i;
//     }
//   }
//   return -1;
// }

MEMFILE* memopen(MEMFILE_descriptor desc, int flags)
{
  // MEMFILE* stream = malloc(sizeof(MEMFILE));
  MEMFILE* stream = calloc(1,sizeof(MEMFILE));
  stream->file_start = desc.file_start;
  stream->cursor = desc.file_start;
  stream->file_len = desc.file_len;
  stream->flags = flags;

  return stream;
}


int memclose(MEMFILE* stream)
{
  free(stream);
  return 0;
}

size_t memread(void* ptr, size_t size, size_t nmemb, MEMFILE* stream)
{
  if((stream->flags & MEM_R) != 0)
  {
    int byte_size = size*nmemb;
    int max_byte_size = (stream->file_start + stream->file_len) - stream->cursor;
    int cpy_size = (byte_size > max_byte_size) ? max_byte_size : byte_size;

    memcpy(ptr, stream->cursor, cpy_size);
    stream->cursor += cpy_size;
    return cpy_size;
  }
  else
  {
    return 0;
  }
}

size_t memwrite(const void* ptr, size_t size, size_t nmemb, MEMFILE* stream)
{
  if((stream->flags & MEM_W) != 0)
  {
    int byte_size = size*nmemb;
    int max_byte_size = (stream->file_start + stream->file_len) - stream->cursor;
    int cpy_size = (byte_size > max_byte_size) ? max_byte_size : byte_size;

    memcpy(stream->cursor, ptr, cpy_size);
    stream->cursor += cpy_size;
    return cpy_size;
  }
  else
  {
    return 0;
  }
}
