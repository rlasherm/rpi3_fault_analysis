/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   kevin
* @Last modified time: 08-12-2016
* @License: GPL
*/
#ifndef GPIO_H
#define GPIO_H

#include "types.h"

#define GPIO_BASE 0x3F200000

//GPIO reg addresses
#define GPIO_GPFSEL0    0
#define GPIO_GPSET0     7
#define GPIO_GPCLR0     10
#define GPIO_GPLEV0     13

#define GPFSEL1 0x3F200004
#define GPSET0  0x3F20001C
#define GPCLR0  0x3F200028

//Function selection
#define FSEL_INPUT    0
#define FSEL_OUTPUT   1
#define FSEL_ALT0     4
#define FSEL_ALT1     5
#define FSEL_ALT2     6
#define FSEL_ALT3     7
#define FSEL_ALT4     3
#define FSEL_ALT5     2

//Special pin_select
#define TRIG_PIN      16

bool set_pin_function(unsigned int pin_select, unsigned int type);
bool write_pin(unsigned int pin_select, bool value);
bool read_pin(unsigned int pin_select);
void toggle_pin(unsigned int pin_select);

void turn_on_LED(void);
void turn_off_LED(void);
void toggle_LED(void);

void error_code_LED(unsigned int error);

void fast_trig_up(void);
void fast_trig_down(void);

#endif
