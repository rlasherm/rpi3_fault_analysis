/**
* @Author: kevin, ronan
* @Date:   09-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-09-2016
* @License: GPL
*/

#ifndef JTAG_H
#define JTAG_H

int initiate_jtag ( void );

#endif
