/**
* @Author: Lashermes Ronan <ronan>
* @Date:   09-02-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-02-2017
* @License: MIT
*/



#ifndef PRINTF_H
#define PRINTF_H

#include <stdarg.h>
#include <stddef.h>


int printf(const char *fmt, ...);
static int snprintf(char *buf, const char *fmt, va_list args);

#endif
