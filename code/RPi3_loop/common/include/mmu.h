/**
* @Author: Lashermes Ronan <ronan>
* @Date:   06-02-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 06-02-2017
* @License: MIT
*/



#ifndef MMU_H
#define MMU_H

#include <stddef.h>
#include "types.h"

#define MEGABYTE			0x100000

#define ARMV8MMU_GRANULE_SIZE		0x10000		// 64KB
#define ARMV8MMU_TABLE_ENTRIES		8192

// Level 2
struct TARMV8MMU_LEVEL2_TABLE_DESCRIPTOR_struct
{
	u64	Value11		: 2,		// set to 3
		Ignored1	: 14,		// set to 0
		TableAddress	: 32,		// table base address [47:16]
		Reserved0	: 4,		// set to 0
		Ignored2	: 7,		// set to 0
		PXNTable	: 1,		// set to 0
		UXNTable	: 1,		// set to 0
		APTable		: 2,
#define AP_TABLE_ALL_ACCESS		0
		NSTable		: 1;		// RES0, set to 0
}
PACKED;

typedef struct TARMV8MMU_LEVEL2_TABLE_DESCRIPTOR_struct TARMV8MMU_LEVEL2_TABLE_DESCRIPTOR;

#define ARMV8MMUL2TABLEADDR(addr)	(((addr) >> 16) & 0xFFFFFFFF)
#define ARMV8MMUL2TABLEPTR(table)	((void *) ((table) << 16))

struct TARMV8MMU_LEVEL2_BLOCK_DESCRIPTOR_struct
{
	u64	Value01		: 2,		// set to 1
		//LowerAttributes	: 10,
			AttrIndx	: 3,	// [2:0], see MAIR_EL1
			NS		: 1,	// RES0, set to 0
			AP		: 2,	// [2:1]
#define ATTRIB_AP_RW_EL1		0
#define ATTRIB_AP_RW_ALL		1
#define ATTRIB_AP_RO_EL1		2
#define ATTRIB_AP_RO_ALL		3
			SH		: 2,	// [1:0]
#define ATTRIB_SH_NON_SHAREABLE		0
#define ATTRIB_SH_OUTER_SHAREABLE	2
#define ATTRIB_SH_INNER_SHAREABLE	3
			AF		: 1,	// set to 1, will fault otherwise
			nG		: 1,	// set to 0
		Reserved0_1	: 17,		// set to 0
		OutputAddress	: 19,		// [47:29]
		Reserved0_2	: 4,		// set to 0
		//UpperAttributes	: 12
			Continous	: 1,	// set to 0
			PXN		: 1,	// set to 0, 1 for device memory
			UXN		: 1,	// set to 1
			Ignored		: 9;	// set to 0
}
PACKED;

typedef struct TARMV8MMU_LEVEL2_BLOCK_DESCRIPTOR_struct TARMV8MMU_LEVEL2_BLOCK_DESCRIPTOR;

#define ARMV8MMU_LEVEL2_BLOCK_SIZE	(512 * MEGABYTE)
#define ARMV8MMUL2BLOCKADDR(addr)	(((addr) >> 29) & 0x7FFFF)
#define ARMV8MMUL2BLOCKPTR(block)	((void *) ((table) << 29))

struct TARMV8MMU_LEVEL2_INVALID_DESCRIPTOR_struct
{
	u64	Value0		: 1,		// set to 0
		Ignored		: 63;
}
PACKED;

typedef struct TARMV8MMU_LEVEL2_INVALID_DESCRIPTOR_struct TARMV8MMU_LEVEL2_INVALID_DESCRIPTOR;

union TARMV8MMU_LEVEL2_DESCRIPTOR_union
{
	TARMV8MMU_LEVEL2_TABLE_DESCRIPTOR	Table;
	TARMV8MMU_LEVEL2_BLOCK_DESCRIPTOR	Block;
	TARMV8MMU_LEVEL2_INVALID_DESCRIPTOR	Invalid;
}
PACKED;

typedef union TARMV8MMU_LEVEL2_DESCRIPTOR_union TARMV8MMU_LEVEL2_DESCRIPTOR;

// Level 3
struct TARMV8MMU_LEVEL3_PAGE_DESCRIPTOR_struct
{
	u64	Value11		: 2,		// set to 3
		//LowerAttributes	: 10,
			AttrIndx	: 3,	// [2:0], see MAIR_EL1
			NS		: 1,	// RES0, set to 0
			AP		: 2,	// [2:1]
			SH		: 2,	// [1:0]
			AF		: 1,	// set to 1, will fault otherwise
			nG		: 1,	// set to 0
		Reserved0_1	: 4,		// set to 0
		OutputAddress	: 32,		// [47:16]
		Reserved0_2	: 4,		// set to 0
		//UpperAttributes	: 12
			Continous	: 1,	// set to 0
			PXN		: 1,	// set to 0, 1 for device memory
			UXN		: 1,	// set to 1
			Ignored		: 9	// set to 0
		;
}
PACKED;

typedef struct TARMV8MMU_LEVEL3_PAGE_DESCRIPTOR_struct TARMV8MMU_LEVEL3_PAGE_DESCRIPTOR;

#define ARMV8MMU_LEVEL3_PAGE_SIZE	0x10000
#define ARMV8MMUL3PAGEADDR(addr)	(((addr) >> 16) & 0xFFFFFFFF)
#define ARMV8MMUL3PAGEPTR(page)		((void *) ((page) << 16))

struct TARMV8MMU_LEVEL3_INVALID_DESCRIPTOR_struct
{
	u64	Value0		: 1,		// set to 0
		Ignored		: 63;
}
PACKED;

typedef struct TARMV8MMU_LEVEL3_INVALID_DESCRIPTOR_struct TARMV8MMU_LEVEL3_INVALID_DESCRIPTOR;

union TARMV8MMU_LEVEL3_DESCRIPTOR_union
{
	TARMV8MMU_LEVEL3_PAGE_DESCRIPTOR	Page;
	TARMV8MMU_LEVEL3_INVALID_DESCRIPTOR	Invalid;
}
PACKED;

typedef union TARMV8MMU_LEVEL3_DESCRIPTOR_union TARMV8MMU_LEVEL3_DESCRIPTOR;

#define ATTRINDX_NORMAL		0
#define ATTRINDX_DEVICE		1
#define ATTRINDX_COHERENT	2

#define MEM_DEVICE_START 0x3F000000

// System registers
#define SCTLR_EL1_WXN		(1 << 19)		// SCTLR_EL1
#define SCTLR_EL1_I		(1 << 12)
#define SCTLR_EL1_C		(1 << 2)
#define SCTLR_EL1_M		(1 << 0)

#define TCR_EL1_IPS__SHIFT	32			// TCR_EL1
#define TCR_EL1_IPS__MASK	(7UL << 32)
	#define TCR_EL1_IPS_4GB		0UL
#define TCR_EL1_EPD1		(1 << 23)
#define TCR_EL1_A1		(1 << 22)
#define TCR_EL1_TG0__SHIFT	14
#define TCR_EL1_TG0__MASK	(3 << 14)
	#define TCR_EL1_TG0_64KB	1
#define TCR_EL1_SH0__SHIFT	12
#define TCR_EL1_SH0__MASK	(3 << 12)
	#define TCR_EL1_SH0_INNER	3
#define TCR_EL1_ORGN0__SHIFT	10
#define TCR_EL1_ORGN0__MASK	(3 << 10)
	#define TCR_EL1_ORGN0_WR_BACK	3
#define TCR_EL1_IRGN0__SHIFT	8
#define TCR_EL1_IRGN0__MASK	(3 << 8)
	#define TCR_EL1_IRGN0_WR_BACK	3
#define TCR_EL1_EPD0		(1 << 7)
#define TCR_EL1_T0SZ__SHIFT	0
#define TCR_EL1_T0SZ__MASK	(0x3F << 0)
	#define TCR_EL1_T0SZ_4GB	32

// #define LOG2_PAGE_SIZE 12
#define LOG2_PAGE_SIZE 16
#define PAGE_SIZE (1 << LOG2_PAGE_SIZE) //due to how peripherals are addressed (each has a 4K memory region reserved)

#define LOG2_IA 32 //input addresses on 32 bits
#define LOG2_OA 32 //output addresses on 32 bits

typedef struct mmu_region_struct {
  void* start_add;
  size_t len;
  //TODO attributes
} mmu_region;

typedef struct mmu_context_struct {
  void* ttbr;
  unsigned long mair;
  unsigned long tcr;
} mmu_context;

TARMV8MMU_LEVEL2_DESCRIPTOR *createLevel2Table (void);
TARMV8MMU_LEVEL3_DESCRIPTOR *createLevel3Table (u64 nBaseAddress);


//reserve some space in memory (of len byte_space, starting from cursor).
//return starting address of the reserved space (fullfilling alignement) if valid
//return NULL if end of memory (end_mem) is reaches
void* reserve_aligned_space(size_t byte_space, size_t alignement, void** cursor, void* end_mem);

//pages_res is the memory region reserved for pages
//if the region is too small, the method return a NULL pointer.
mmu_context mmu_init_tables(mmu_region pages_res);

void mmu_enable(mmu_context context);


#endif
