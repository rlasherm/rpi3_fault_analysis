/**
* @Author: Lashermes Ronan <ronan>
* @Date:   13-01-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 13-01-2017
* @License: MIT
*/



#ifndef INIT_H
#define INIT_H

void set_vbar_elx(unsigned int el, void* address);

#endif
