# RPi3 setup

The Raspberry Pi 3 setup is a bit complex.
4 boards are used:
- the RPi3 board itself,
- the FTDI Friend for communication between the host and the RPi3,
- the BusBlaster v4.1a for JTAG debugging,
- the Digilent Arty FPGA board with Triggy IP for automated reset (this tutorial does not deal with how to put the Triggy IP).

## Connections

The boards must be adequatelly powered: RPi3 has dedicated powering connection.

### Triggy to host

Triggy is connected to the host with its USB connection, both in charge of communications and powering.

### RPi3 to host (through FTDI Friend)

Communications between the RPi3 and the host use Serial-Over-USB thanks to the FTDI Friend board.
The RPi3 pinout is here
![RPi pinout](./Figures/raspberry-pi-pinout.png).

Connect:

RPi3 pin|FTDI Friend pin
---|---
GPIO14 (Pin 8)|TX
GPIO15 (Pin 10)|RX
Any Ground|Ground

![FTDI Friend](./Figures/ftdi_friend.jpg)

### RPi3 to Triggy (for reset)

The reset wires must be soldered on the RPi3 as seen here, the black and green wires in the top right corner.
![RPi3](./Figures/rpi3_connector.jpg).

Then these two wires must be connected to the Triggy board [manual](https://reference.digilentinc.com/reference/programmable-logic/arty/reference-manual).

RPi3 pin|Triggy pin|
Reset|JB pin 1|
|Any Ground|Any Ground (e.g. JB pin 5.)

![Triggy](./Figures/triggy.jpg).

### RPi3 to BusBlaster (JTAG)

The first step here is to take a good coffee and stay calm.
You will have to connect all JTAG wires individually both on the BusBlaster board and on the RPi3 board.

![RPi3](./Figures/rpi3_connector.jpg)
![RPi3-2](./Figures/rpi3_connector.jpg)
![BusBlaster](./Figures/bus_blaster.jpg)
![BusBlaster-2](./Figures/bus_blaster_view.jpg)

RPi3 pin | Bus Blaster pin
---|---
GPIO4 (Pin 7, Alt5)|TDI
GPIO27 (Pin 13, Alt4)|TMS
GPIO22 (Pin 15, Alt4)|TRST
GPIO23 (Pin 16, Alt4)|RTCK
GPIO24 (Pin 18, Alt4)|TDO
GPIO25 (Pin 22, Alt4)|TCK
Any Ground|Any Ground

Now you can finish your coffee.

### RPi3 to Faustine platform

This is the trigger GPIO.

RPi3 pin| Faustine signal
---|---
GPIO16 (Pin 36)|Trigger
Any Ground|Ground

## Burning the RPi3 image

The application is burnt into a sd-card, on which the RPi3 boots from.
To burn the image, use the [makefile](../build/Makefile) and modify the path to the SD_CARD in the related variable.
If you use the sd card for the first time, use the command
```bash
make prepare-burn
```
To properly erase the sd card and put the booting file.
Then
```bash
make burn
```
will copy the application image into the sd card. Only this action is required from now on if you only want to modify the application.
Put the sd card into the RPi3 and reset. Voilà!

## Triggy preparation

Triggy is the IP on the Arty FPGA board responsible for hard reseting the card if necessary.
By default, the reset action is continously enforced when powering up the FPGA.
To "release" the RPi3, the Triggy board must be properly configured.

Trigger 0 must be kept high by default by setting its idle polarity to high.
For more info, cf the [Triggy doc](../../../Documentation/Guides/Triggy/triggy.md).

THIS STEP IS REQUIRED FOR THE RPI3 TO WORK!

## Communicating with the RPi3

To communicate with the RPi3 board, simple open a serial terminal on the host with the proper configuration:
- proper port,
- 115200 bauds,
- 8 bits data,
- no parity,
- 1 stop bit

Send 't' to test connection, you should see a "RPI3 DIV test OK" answer or something similar.
If nothing happens, either the application in the RPi3 image is not programmed to answer the 't' command, or the Triggy setup is incorrect (the RPi3 is continously rebooting in this case).
