/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "init.h"
#include "jtag.h"
#include "mem.h"
#include "interrupt_handler.h"
#include "boot.h"
#include "secure_boot.h"
#include "mmu.h"
#include "myaes.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
// #include "mbedtls/error.h"

#define DATA_SIZE 16

unsigned char vec[4];
char text[256];

void gpio_init() {
  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}

void initialise_monitor_handles(void)
{

}

void boot_init ( void )
{

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  // call construtors of static objects
	extern void (*__init_start) (void);
	extern void (*__init_end) (void);
	for (void (**pFunc) (void) = &__init_start; pFunc < &__init_end; pFunc++)
	{
		(**pFunc) ();
	}


  gpio_init();
  //fast_trig_up();
  //fast_trig_down();

  // initiate_jtag();
  timer_init();
  uart_init();


  // uart_print("BOOTLOADER: Init...\n\r");

  mmu_region page_mem = { .start_add = &_pages_start, .len = (&_pages_end - &_pages_start)};
  mmu_context mmu_ctx = mmu_init_tables(page_mem);
  mmu_enable(mmu_ctx);

  enable_icache_EL3();

  //write vbar_el3
  set_vbar_elx(3, &vbar_elx_vec);
  set_vbar_elx(2, &vbar_elx_vec);
  set_vbar_elx(1, &vbar_elx_vec);

  //start caches
  // start_l1cache();
  // start_vfp();

  // start other cores (initialize stack vectors)
  // start_other_core (MBXSETC13, core_wrapper);
  // start_other_core (MBXSETC23, core_wrapper);
  // start_other_core (MBXSETC33, core_wrapper);

  //call main after preinit
  boot_main();
}


void loop(void) {
  int i = 0;
  int j = 0;
  int cnt = 0;
  memset(text, 0, 256);

  
  fast_trig_up();
  wait_us(2);
  invalidate_icache();
  //trig up

  // init_registers();
  
  for(i = 0;i<20; i++) {
    for(j = 0;j<20;j++) {
      // register_transfer();

      cnt++;
    }
  }
  fast_trig_down();

  // if(!verif_registers()) {
    // wait_us(2);
  //   cnt = 1;
  // }

  //trig down
  sprintf(text, "i=%d j=%d cnt=%d\n", i, j, cnt);
  uart_print(text);

  
}

/**************************************************/
/*                  Main                          */
/**************************************************/

int boot_main ( void )
{

  uart_print("Shall we play a game?\n");


  while(1)
  {
    // invalidate_icache();
    char cmd = uart_recv();

    switch(cmd) {
      case 't':
        uart_print("RPi3 Test OK\n");
        break;

      case 'l':
        loop();
        break;

      
      default:
        //uart_print("Unknown command\n");
        //printf("Unknown command: %02x\n", cmd);
        break;
    }

  }



  return 0;
}
