# Experiments on The Loop

These are calibration experiment initially :
- Exp1 to search spatial PoI (crashes only, wo cache flush)
- Exp2 to search spatial PoI (crashes only, with cache flush)
- Exp3: manual parameters exploration to see if crashes can be transformed in faults -> first fault
Beware the code was compiled (due to an error) in 01 -> the fault value has no meaning
- Exp4: spatial exploration for faults
- Exp5: temporal explaration for faults
- Exp6: spatial exploration for faults (with another delay parameter)
- Exp7: exploring the effect of a fault (MMU impact)
- Exp8: exploring the effect of a fault (MMU impact)

- Exp9: trying to fault loop
- Exp10: nice MMU fault
- Exp11: nice L1I fault

- Exp12|13: XY scan to find where we can inject icache faults.
- Exp14: L2 fault

## MMU

- ExpMMU6: exploring the impact of the fault on address translations and page tables values
