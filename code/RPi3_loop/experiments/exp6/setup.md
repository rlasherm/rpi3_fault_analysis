# Searching for points of interest

0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


# Target

Loop, with cache flush.

# Settings

Spatial exploration

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm, delay = 1120ns
post_tempo=150ms (min delay between fault injections)


# Qualitative observations



