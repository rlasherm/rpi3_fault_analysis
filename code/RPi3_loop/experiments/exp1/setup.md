# Searching for points of interest

0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


X = [-13mm; 0mm]
Y = [0mm; +13mm]

# Target

Loop, no cache flush.

# Settings

Probre: RF R 0.3-3
Sinus 275MHz, 7 periods, -7dBm (before amp)
Delay = 1004ns

# Qualitative observations

Trigger signal is modified by faults, with memory effect.
It is either too short (500ns) or too long (8ms), instead of 5us. No explications at this point (force cache flush ?).
Application is correct.

