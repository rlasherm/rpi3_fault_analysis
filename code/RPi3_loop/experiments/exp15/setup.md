
0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


# Target

Loop, with register transfer to evaluate fault model

# Settings

X=-6mm
Y=3.5mm

Delay = exploration

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm

# Qualitative observations



