
0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


# Target

Loop, with cache flush. -> 46us

# Settings

X = -12mm
Y = 7mm

Varying delay for result.csv
delay @ 3144ns for telnet_icache_faultXXX.log

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm

# Qualitative observations



