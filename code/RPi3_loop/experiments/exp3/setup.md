# Searching for points of interest

0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0

X = -7mm
Y = 5mm

# Target

Loop, with cache flush.

# Settings

Manual parameters exploration to see if crashes can be transformed in faults

Probre: RF R 0.3-3
Initial: 
- Crashes: Sinus 275MHz, 7 periods, -7dBm (before amp), delay = 0ns
- First faults: Sinus 275MHz, 2 periods, -10dBm, delay = 1244ns, pos = (-10mm, 5mm) 

# Qualitative observations



