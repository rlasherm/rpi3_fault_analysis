# Searching for points of interest

0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0

X = -8mm
Y = 5mm

# Target

Loop, with cache flush.

# Settings

Manual parameters exploration to see if crashes can be transformed in faults

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm, exploring delay
post_tempo=150ms (min delay between fault injections)


# Qualitative observations



