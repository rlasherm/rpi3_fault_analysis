
0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0


# Target

Loop, without cache flush. -> us

# Settings

X = -11mm
Y = 7mm

Varying delay.
m1 @ 25400ns

Probe: RF U 5-2
Sinus 275MHz, 1 period, -14dBm

# Qualitative observations



