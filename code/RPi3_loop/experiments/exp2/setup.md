# Searching for points of interest

0,13    -13,10
|-------|
|*      |
|       |
|       |
|-------|
0,0     -13,0

X = [-13mm; 0mm]
Y = [0mm; +13mm]

# Target

Loop, with cache flush.

# Settings

Probre: RF R 0.3-3
Sinus 275MHz, 7 periods, -7dBm (before amp)
Delay = 0ns

# Qualitative observations

Correct trigger duration = 5.5us



