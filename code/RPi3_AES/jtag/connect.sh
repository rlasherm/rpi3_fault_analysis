# @Author: Lashermes Ronan <ronan>
# @Date:   18-01-2017
# @Email:  ronan.lashermes@inria.fr
# @Last modified by:   ronan
# @Last modified time: 18-01-2017
# @License: MIT

#http://pinout.xyz/

#Open On-Chip Debugger 0.10.0+dev-00207-g41092636 (11/2017) 

# openocd -f interface/ftdi/dp_busblaster.cfg -f bcmrpi2.cfg
openocd -f interface/ftdi/dp_busblaster.cfg -f rpi3.cfg
#openocd -f interface/ftdi/dp_busblaster.cfg -f rpi3_new.cfg
