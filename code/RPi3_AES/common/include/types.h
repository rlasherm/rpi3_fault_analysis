/**
* @Author: ronan
* @Date:   01-09-2016
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 12-09-2016
* @License: GPL
*/



#ifndef TYPES_H
#define TYPES_H

#include <stdbool.h>

#define PACKED		__attribute__ ((packed))
#define	ALIGN(n)	__attribute__ ((aligned (n)))
#define NOOPT		__attribute__ ((optimize (0)))
#define MAXOPT		__attribute__ ((optimize (3)))

// big endian (to be used for constants only)
#define BE(value)	((((value) & 0xFF00) >> 8) | (((value) & 0x00FF) << 8))

typedef unsigned char byte;

typedef unsigned char		u8;
typedef unsigned short		u16;
typedef unsigned int		u32;
typedef unsigned long		u64;

typedef signed char		s8;
typedef signed short		s16;
typedef signed int		s32;
typedef signed long		s64;

typedef long			intptr;
typedef unsigned long		uintptr;

#endif
