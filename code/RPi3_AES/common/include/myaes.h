/**
* @Author: hélène, ronan
* @Date:   12-09-2016
* @Email:  helene.lebouder@inri.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 12-09-2016
* @License: GPL
*/



#ifndef AES_H
#define AES_H

#define DATA_SIZE 16
#define STATE_ROW_SIZE 4
#define ROUND_COUNT 10

void init_caches(unsigned char* ciphertext, unsigned char* plaintext , unsigned char* key);
void AESEncrypt(unsigned char* ciphertext, unsigned char* plaintext , unsigned char* key);

void AddRoundKey( unsigned char tableau_1 [][STATE_ROW_SIZE], unsigned char tableau_2 [][STATE_ROW_SIZE]);
void SubBytes(unsigned char state[][STATE_ROW_SIZE]);
void ShiftRows(unsigned char state[][STATE_ROW_SIZE]);
void MixColumns(unsigned char state [][STATE_ROW_SIZE]);

void KeyGen(unsigned char keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], unsigned char master_key [][STATE_ROW_SIZE]);
void ColumnFill( unsigned char cle[][STATE_ROW_SIZE][STATE_ROW_SIZE] , int round);
void OtherColumnsFill( unsigned char cle [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);
void GetRoundKey( unsigned char round_key [][STATE_ROW_SIZE], unsigned char keys [][STATE_ROW_SIZE][STATE_ROW_SIZE], int round);

void MessageToState(unsigned char state [][STATE_ROW_SIZE], unsigned char message []);
void StateToMessage(unsigned char message [],unsigned char state [][STATE_ROW_SIZE]);
// void RecopieTableau( unsigned char tableau_in [][STATE_ROW_SIZE], unsigned char tableau_out [][STATE_ROW_SIZE], int longueur, int largeur);
// void RecopieVecteur( unsigned char vecteur_in [], unsigned char vecteur_out [], int longueur);
void MCMatrixColumnProduct(unsigned char colonne []);
unsigned char gmul(unsigned char a, unsigned char b);

extern const unsigned char rcon [10];

extern const unsigned char sboxtab [256];
extern const unsigned char invsbox [256];

extern unsigned char trigger_sel;
extern int max;

#endif
