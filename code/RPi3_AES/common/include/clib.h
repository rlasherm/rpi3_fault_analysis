/**
* @Author: Lashermes Ronan <ronan>
* @Date:   09-02-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-02-2017
* @License: MIT
*/



#ifndef CLIB_H
#define CLIB_H

#include <stddef.h>

#define MAX_STRNLEN 2048

size_t strlen(const char *s);
int puts(const char *s);



#endif
