/**
* @Author: kevin, ronan
* @Date:   02-09-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 05-09-2016
* @License: GPL
*/

#ifndef TIMER_H
#define TIMER_H

#define TIMER_LO 0x3F003004
#define TIMER_HI 0x3F003008
#define TIMER_BASE 0x3F003000
#define TIMER_CNT 0x3F003004


#include <stdlib.h>
#include <stdint.h>

void timer_init(void);
unsigned int get_timer_tick(void);
unsigned long get_timer_tick64(void);
void wait_us(unsigned long);

#endif
