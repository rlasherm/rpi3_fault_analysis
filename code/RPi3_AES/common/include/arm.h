/**
* @Author: kevin, ronan
* @Date:   31-08-2016
* @Email:  sebanjila.bukasa@inria.fr, ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/

#ifndef ARM_H
#define ARM_H

extern char _heap_start;
extern char _heap_end;
extern char _stack_top;
extern char _stack_bottom;
extern char _pages_start;
extern char _pages_end;

//Write a value at an address
extern void put32(unsigned int address, unsigned int value);
//Read the value at this address
extern volatile unsigned int get32(unsigned int address);

//Write a value at an address
extern void put64(unsigned long address, unsigned long value);
//Read the value at this address
extern volatile unsigned long get64(unsigned long address);

extern void DataSyncBarrier(void);
extern void dummy (void);//do nothing
// extern unsigned int getMPIDR(); //obtain MPIDR value to see which core is in execution
// extern unsigned int getACTLR( void ); //get ACTLR of the current core
// extern unsigned int getCPSR( void ); //get CPSR
extern void* getLR( void ); //get LR
extern unsigned long getEL( void ); //get EL
extern unsigned long getTCR_EL3(void);
extern unsigned long getSPSR_EL3(void);//get SPSR_EL3
extern unsigned int getCPTR_EL3(void);
extern unsigned long getSCTLR_EL2(void);//get SCTLR_EL2
extern void* getSP(void);

extern void* getFAR_EL3(void);
extern void* getELR_EL3(void);
extern unsigned int getESR_EL3(void);

extern unsigned long getSCR_EL3(void);//get SCR_EL3 (bit 0 is NS bit)
extern void switchUNSEC(void);
extern void switchSEC(void);

extern void smc_call(void);

extern void branchto(void* add);
extern void branchto_arg(void* add, void* r0);

extern void invalidate_icache(void);
extern void enableMMU_EL3(void* ttbr, unsigned long tcr);
extern void enable_icache_EL3(void);
extern void setMAIR_EL3(unsigned long val);

//interrupts
extern void vbar_elx_vec();
// extern unsigned long vbar_el2;
// extern unsigned long vbar_el1;
extern void setVBAR_EL3(void* add);//place vector table for EL3
extern void setVBAR_EL2(void* add);//place vector table for EL2
extern void setVBAR_EL1(void* add);//place vector table for EL1


extern void test(void);
extern void EL3toEL2(void* jump_address);
extern void EL2toEL1(void* jump_address);

extern void invalidate_dcache (unsigned long address);
extern long MMU_translate ( long address );
// extern long get_address_translate ( void );
extern void flush_TLB (void);
extern long read_ttbr0_el3(void);

extern void init_registers(void);
extern void register_transfer();
extern long verif_registers();

// extern unsigned int* getSP(); //get SP
// extern unsigned int getSCR( void ); //get SCR
// extern unsigned int getISR( void ); //get ISR
// extern unsigned int getMVBAR( void ); //get MVBAR
// extern unsigned int getNSACR( void ); //get NSACR
// extern unsigned int getSDER( void ); //get SDER
// extern unsigned int getVBAR( void ); //get VBAR
//
// extern void start_l1cache(void);//start caches
// extern void start_vfp(void);//start Vector Floating Point
// extern void core_wrapper( void ); //wrap to initiate cores
// extern void core_func_wrapper( void ); //wrap to assign functions to cores
// extern void ind_shutdwn( void ); //set an individual core into wait for interrupt mode (WFI)
// extern void enb_smp( void ); //enable synchronous multiprocessing
// extern void dis_smp( void ); //disable synchronous multiprocessing
// extern void wfi_func( unsigned int core ); //wait and execute functions
// extern void other_core_loop( void);
// extern void smc_call( void ); //get SP


/* Usage:
 op contains the operation wanted:
 0 : change security state
 1 : change processor current mode
 2 : copy monitor CPSR to C0 mailbox 3

 data contains the value
 to change security state :
   - 1 change secure state
   - 0 does nothing
 to change core mode send the requested value of the cpsr
*/
// extern void pre_smc_call(unsigned int op, unsigned int data);

#endif
