/**
* @Author: Lashermes Ronan <ronan>
* @Date:   09-02-2017
* @Email:  ronan.lashermes@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-02-2017
* @License: MIT
*/



#include "clib.h"
#include "uart.h"
#include <stdio.h>

size_t strlen(const char *str)
{
  int i;
  for(i=0; i < MAX_STRNLEN; i++)
  {
    if(str[i] == '\0')
    {
      return i;
    }
  }
  return -1;
}



int puts(const char *s)
{
  int i = 0;
  while(s[i])  //standard c idiom for looping through a null-terminated string
  {
    if( s[i] == EOF)  //if we got the EOF value from writing the char
    {
      return EOF;
    }
    else {
      uart_send((unsigned int) s[i]);
    }
    i++;
  }
  return 1;
}

// int putchar(int c)
// {
//   uart_send((unsigned int)c);
//   if(c == EOF)
//   {
//     return EOF;
//   }
//   else
//   {
//     return 1;
//   }
// }
