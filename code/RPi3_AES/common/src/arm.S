// @Author: Lashermes Ronan <ronan>
// @Date:   13-01-2017
// @Email:  ronan.lashermes@inria.fr
// @Last modified by:   ronan
// @Last modified time: 13-01-2017
// @License: MIT



.section ".text.arm"

.globl put32
put32:
  str w1,[x0]
  ret

.globl put64
put64:
  str x1,[x0]
  ret

.globl get32
get32:
  ldr w0,[x0]
  ret

.globl get64
get64:
  ldr x0,[x0]
  ret


.globl getLR
getLR:
  mov x0,x30
  ret

.globl getSP
getSP:
//  str sp,[sp]
  mov x0,SP
  ret

.globl getEL
getEL:
  mrs x1, CurrentEL
  lsr x0,x1,#2
  ret

//only in EL3
.globl getSCR_EL3
getSCR_EL3:
  mrs x0, SCR_EL3
  ret

.globl getELR_EL3
getELR_EL3:
  mrs x0, elr_el3
  ret

.globl getFAR_EL3
getFAR_EL3:
  mrs x0, far_el3
  ret

.globl getESR_EL3
getESR_EL3:
  mrs x0, esr_el3
  ret

.globl getSPSR_EL3
getSPSR_EL3:
  mrs x0, SPSR_EL3
  ret

.globl getCPTR_EL3
getCPTR_EL3:
  mrs x0, CPTR_EL3
  ret

.globl getSCTLR_EL2
getSCTLR_EL2:
  mrs x0, SCTLR_EL2
  ret

.globl switchUNSEC
switchUNSEC:
  mrs x0, SCR_EL3
  mov x1, #1
  bfi x0, x1, #0, #1
  msr SCR_EL3, x0
  ret

.globl switchSEC
switchSEC:
  mrs x0, SCR_EL3
  mov x1, #0
  bfi x0, x1, #0, #1
  msr SCR_EL3, x0
  ret

.globl setVBAR_EL3
setVBAR_EL3:
  msr VBAR_EL3, x0
  ret

.globl setVBAR_EL2
setVBAR_EL2:
  msr VBAR_EL2, x0
  ret

.globl setVBAR_EL1
setVBAR_EL1:
  msr VBAR_EL1, x0
  ret

.globl setMAIR_EL3
setMAIR_EL3:
  msr MAIR_EL3, x0
  ret

.globl getTCR_EL3
getTCR_EL3:
  mrs x0, tcr_el3
  ret

.globl enableMMU_EL3
enableMMU_EL3:
  MSR TTBR0_EL3, X0               // Set TTBR0
  MSR TCR_EL3, X1                 // Set TCR
  ISB                             // The ISB forces these changes to be seen before /
                                  // the MMU is enabled.
  MRS X0, SCTLR_EL3               // Read System Control Register configuration data
  ORR X0, X0, #1                  	// Set [M] bit and enable the MMU.
  ORR X0, X0, #4                //Set C
  ORR X0, X0, #0x1000              //Set I
  MSR SCTLR_EL3, X0               // Write System Control Register configuration data
  ISB                             // The ISB forces these changes to be seen by the /
                                  // next instruction
  ret

.globl DataSyncBarrier
DataSyncBarrier:
  ISB
  ret

.globl enable_icache_EL3
enable_icache_EL3:
  //activate I cache
  mrs x3, SCTLR_EL3
  orr x3, x3,#0x1000
  msr SCTLR_EL3, x3
  ret

.globl invalidate_icache
invalidate_icache:
  ic iallu
  isb
  ret

//arg: x0 is address to jump to in EL2
.globl EL3toEL2
EL3toEL2:
  mov	x1, #0x5b1	/* Non-secure EL0/EL1 | HVC | 64bit EL2 */
  //mov	x1, #0x531	/* Non-secure EL0/EL1 | HVC | 64bit EL2 */
  msr	scr_el3, x1
  msr	cptr_el3, xzr	/* Disable coprocessor traps to EL3 */
  mov	x1, #0x33ff
  msr	cptr_el2, x1	/* Disable coprocessor traps to EL2 */

  /* Initialize SCTLR_EL2 */
  msr	sctlr_el2, xzr

  /* Return to the EL2_SP2 mode from EL3 */
  mov	x1, sp
  msr	sp_el2, x1	/* Migrate SP */
  mrs	x1, vbar_el3
  msr	vbar_el2, x1	/* Migrate VBAR */
  mov	x1, #0x3c9
  msr	spsr_el3, x1	/* EL2_SP2 | D | A | I | F */
  msr	elr_el3, x0
  eret

//arg: x0 is address to jump to in EL1
.globl EL2toEL1
EL2toEL1:
  /* Initialize Generic Timers */
  mrs	x2, cnthctl_el2
  orr	x2, x2, #0x3		/* Enable EL1 access to timers */
  msr	cnthctl_el2, x2
  msr	cntvoff_el2, xzr
  mrs	x2, cntkctl_el1
  orr	x2, x2, #0x3		/* Enable EL0 access to timers */
  msr	cntkctl_el1, x2

  /* Initilize MPID/MPIDR registers */
  mrs	x2, midr_el1
  mrs	x1, mpidr_el1
  msr	vpidr_el2, x2
  msr	vmpidr_el2, x1

  /* Disable coprocessor traps */
  mov	x2, #0x33ff
  msr	cptr_el2, x2		/* Disable coprocessor traps to EL2 */
  msr	hstr_el2, xzr		/* Disable coprocessor traps to EL2 */
  mov	x2, #3 << 20
  msr	cpacr_el1, x2		/* Enable FP/SIMD at EL1 */

  /* Initialize HCR_EL2 */
  mov	x2, #(1 << 31)		/* 64bit EL1 */
  //orr	x2, x2, #(1 << 29)	/* Disable HVC */
  msr	hcr_el2, x2

  /* SCTLR_EL1 initialization */
  mov	x2, #0x0800
  movk	x2, #0x30d0, lsl #16
  msr	sctlr_el1, x2

  /* Return to the EL1_SP1 mode from EL2 */
  mov	x2, sp
  msr	sp_el1, x2		/* Migrate SP */
  mrs	x2, vbar_el2
  msr	vbar_el1, x2		/* Migrate VBAR */
  mov	x2, #0x3c5
  msr	spsr_el2, x2		/* EL1_SP1 | D | A | I | F */
  msr	elr_el2, x0
  eret

.globl smc_call
smc_call:
  //str   x30, [sp, #-16]!
  //msr	elr_el3, x30
  //msr	elr_el2, x30
  //msr	elr_el1, x30
  hvc #0
  //bl uart_beacon
  //ldr   x30, [sp], #16
  ret

.globl branchto
branchto:
  str   x30, [sp, #-16]!
  blr x0
  ldr   x30, [sp], #16
  ret

.globl branchto_arg
branchto_arg:
  str   x30, [sp, #-16]!
  blr x0
  ldr   x30, [sp], #16
  ret


.globl dummy
dummy:
  ret

.globl sink
sink:
  b sink

//.balign 128
.globl test
test:
  str   x30, [sp, #-16]!
  bl default_handler
  ldr   x30, [sp], #16
  ret


.globl MMU_translate
MMU_translate:
  at s1e3r, x0
  mrs x0, PAR_EL1
  ret

/* .globl get_address_translate
get_address_translate:
  mrs x0, PAR_EL1
  ret*/

.globl read_ttbr0_el3
read_ttbr0_el3:
  mrs x0, TTBR0_EL3
  ret


.globl flush_TLB
flush_TLB:
  tlbi alle3
  ret

.globl invalidate_dcache
invalidate_dcache:
  dc ivac, x0
  dsb sy
  dmb sy
  ret

.globl init_registers
init_registers:
  mov x0, #0x1
  mov x1, #0x3
  mov x2, #0x7
  mov x3, #0xF
  mov x4, #0x1F
  mov x5, #0x3F
  mov x6, #0x7F
  mov x7, #0xFF
  ret

.globl verif_registers
verif_registers:
  //cmp x0, #0x1
  //B.NE ne
  //cmp x1, #0x3
  //B.NE ne
  cmp x2, #0x7
  B.NE ne
  cmp x3, #0xF
  B.NE ne
  cmp x4, #0x1F
  B.NE ne
  cmp x5, #0x3F
  B.NE ne
  cmp x6, #0x7F
  B.NE ne
  cmp x7, #0xFF
  B.NE ne
//e:
  mov x0, #1
  ret
ne:
  mov x0, #0
  ret

.globl register_transfer
register_transfer:
  mov x0, x0
  mov x1, x1
  mov x2, x2
  mov x3, x3
  mov x4, x4
  mov x5, x5
  mov x6, x6
  mov x7, x7  
  mov x8, x8
  mov x9, x9
  mov x10, x10
  mov x11, x11
  mov x12, x12
  mov x13, x13
  mov x14, x14
  mov x15, x15
  mov x16, x16
  mov x17, x17
  mov x18, x18
  mov x19, x19
  mov x20, x20
  mov x21, x21
  mov x22, x22
  mov x23, x23  
  mov x24, x24
  mov x25, x25
  mov x26, x26
  mov x27, x27
  mov x28, x28
  mov x29, x29
  mov x30, x30
  mov sp, sp
  ret