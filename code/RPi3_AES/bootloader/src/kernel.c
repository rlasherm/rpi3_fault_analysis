/**
* @Author: ronan, kevin
* @Date:   31-08-2016
* @Email:  ronan.lashermes@inria.fr, sebanjila.bukasa@inria.fr
* @Last modified by:   ronan
* @Last modified time: 09-01-2017
* @License: GPL
*/


#include "types.h"
#include "kernel.h"
#include "arm.h"
#include "gpio.h"
#include "uart.h"
#include "timer.h"
#include "init.h"
#include "jtag.h"
#include "mem.h"
#include "interrupt_handler.h"
#include "boot.h"
#include "secure_boot.h"
#include "mmu.h"
#include "myaes.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
// #include "mbedtls/error.h"

#define DATA_SIZE 16

unsigned char vec[4];
char text[256];

void gpio_init() {
  //set trig pin as output
  set_pin_function(TRIG_PIN, FSEL_OUTPUT);
}

void initialise_monitor_handles(void)
{

}

void boot_init ( void )
{

  //Erase BSS
  unsigned int* bss  = &__bss_start__;
  unsigned int*  bss_end = &__bss_end__ ;

  //Initialize bss section to 0
  while(bss < bss_end) {
    *bss++ = 0;
  }

  // call construtors of static objects
	extern void (*__init_start) (void);
	extern void (*__init_end) (void);
	for (void (**pFunc) (void) = &__init_start; pFunc < &__init_end; pFunc++)
	{
		(**pFunc) ();
	}


  gpio_init();
  //fast_trig_up();
  //fast_trig_down();

  //initiate_jtag();// done in startup
  timer_init();
  uart_init();


  // uart_print("BOOTLOADER: Init...\n\r");

  mmu_region page_mem = { .start_add = &_pages_start, .len = (&_pages_end - &_pages_start)};
  mmu_context mmu_ctx = mmu_init_tables(page_mem);
  mmu_enable(mmu_ctx);

  enable_icache_EL3();

  //write vbar_el3
  set_vbar_elx(3, &vbar_elx_vec);
  set_vbar_elx(2, &vbar_elx_vec);
  set_vbar_elx(1, &vbar_elx_vec);

  //start caches
  // start_l1cache();
  // start_vfp();

  // start other cores (initialize stack vectors)
  // start_other_core (MBXSETC13, core_wrapper);
  // start_other_core (MBXSETC23, core_wrapper);
  // start_other_core (MBXSETC33, core_wrapper);

  //call main after preinit
  boot_main();
}


void loop(void) {
  int i = 0;
  int j = 0;
  int cnt = 0;
  memset(text, 0, 256);

  
  fast_trig_up();
  wait_us(2);
  invalidate_icache();
  //trig up

  // init_registers();
  
  for(i = 0;i<20; i++) {
    for(j = 0;j<20;j++) {
      // register_transfer();

      cnt++;
    }
  }
  fast_trig_down();

  // if(!verif_registers()) {
    // wait_us(2);
  //   cnt = 1;
  // }

  //trig down
  sprintf(text, "i=%d j=%d cnt=%d\n", i, j, cnt);
  uart_print(text);

  
}

/**************************************************/
/*                  Main                          */
/**************************************************/

int boot_main ( void )
{
  unsigned char plaintext[DATA_SIZE];
  unsigned char ciphertext[DATA_SIZE];
  //unsigned char key[DATA_SIZE]={121,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};
  unsigned char key[DATA_SIZE]={0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
  unsigned char dummy_key[DATA_SIZE]={121,27,229,29,20,83,109,221,136,255,194,140,136,168,13,101};

//66 32 43 f6 a8 88 5a 30 8d 31 31 98 a2 e0 37 07 34
  unsigned char plaintext_verif[DATA_SIZE]={0x32, 0x43, 0xf6, 0xa8, 0x88, 0x5a, 0x30, 0x8d, 0x31, 0x31, 0x98, 0xa2, 0xe0, 0x37, 0x07, 0x34};
  unsigned char key_verif[DATA_SIZE]={0x2b, 0x7e, 0x15, 0x16, 0x28, 0xae, 0xd2, 0xa6, 0xab, 0xf7, 0x15, 0x88, 0x09, 0xcf, 0x4f, 0x3c};
  // unsigned char ciphertext_verif[DATA_SIZE]={0x39, 0x25, 0x84, 0x1d, 0x02, 0xdc, 0x09, 0xfb, 0xdc, 0x11, 0x85, 0x97, 0x19, 0x6a, 0x0b, 0x32};

  uart_print("Shall we play a game?\n");


  while(1)
  {
    // invalidate_icache();
    char cmd = uart_recv();

    switch(cmd) {
      case 't':
        uart_print("RPi3 Test OK\n");
        break;

      case 'k'://set key (binary)
        uart_read_vector(key, DATA_SIZE);
        break;

      case 'p'://set Plaintext (binary)
        uart_read_vector(plaintext, DATA_SIZE);
        break;

      case 'i':
        init_caches(ciphertext, plaintext_verif, key_verif);
        uart_send_vector(ciphertext, DATA_SIZE);//transmit ciphertext
        break;

      case 'g'://go!
        AESEncrypt(ciphertext, plaintext, key);
        break;

      case 'c'://get ciphertext
        uart_send_vector(ciphertext, DATA_SIZE);
        break;

      case 'f'://fast mode
        uart_read_vector(plaintext, DATA_SIZE);//receive Plaintext
        AESEncrypt(ciphertext, plaintext, key);//AES
        uart_send_vector(ciphertext, DATA_SIZE);//transmit ciphertext
        break;
      case 'r':
        trigger_sel = uart_recv();
        break;
      

      case 'l':
        loop();
        break;

      
      default:
        //uart_print("Unknown command\n");
        //printf("Unknown command: %02x\n", cmd);
        break;
    }

  }



  return 0;
}
