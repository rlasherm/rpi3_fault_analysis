set terminal latex
set output "sbox_distrib.tex"
set xrange [0:255]
set xlabel "$C^*_0$"
set ylabel "\\rotatebox{90}{Occurences}"
plot 'sbox_distrib.csv' with points pt 2 lc rgb "black" notitle
