from plotter import Plotter, PlotterType

data = [32, 45, 30, 37, 48, 42, 35, 49, 33, 43, 40, 40, 38, 44, 38, 44, 45, 44,
        50, 34, 41, 46, 30, 48, 31, 38, 43, 32, 28, 38, 40, 48, 38, 44, 32, 0,
        33, 78, 44, 37, 31, 45, 37, 41, 29, 37, 50, 43, 35, 50, 43, 36, 41, 29,
        37, 38, 39, 28, 33, 38, 44, 39, 45, 36, 48, 36, 36, 48, 39, 49, 35, 59,
        40, 37, 48, 50, 41, 30, 37, 30, 35, 48, 42, 35, 45, 39, 37, 39, 38, 37,
        44, 44, 42, 29, 49, 37, 40, 58, 33, 33, 39, 42, 32, 32, 35, 31, 34, 34,
        47, 31, 41, 43, 36, 38, 57, 45, 42, 36, 27, 34, 36, 46, 43, 43, 35, 41,
        47, 41, 37, 40, 26, 52, 28, 20, 43, 41, 46, 51, 42, 74, 42, 34, 42, 0,
        40, 43, 50, 33, 31, 31, 40, 36, 33, 45, 33, 31, 35, 30, 44, 40, 34, 34,
        33, 48, 28, 35, 38, 35, 36, 44, 38, 35, 42, 43, 49, 39, 46, 45, 47, 35,
        41, 45, 36, 32, 39, 40, 30, 25, 32, 41, 41, 40, 29, 43, 36, 51, 35, 30,
        40, 40, 33, 43, 51, 42, 37, 38, 36, 32, 33, 44, 36, 38, 31, 44, 44, 33,
        43, 48, 50, 45, 28, 43, 57, 51, 35, 46, 32, 30, 41, 38, 39, 29, 34, 38,
        35, 38, 32, 45, 43, 41, 41, 39, 34, 38, 38, 28, 41, 35, 37, 48, 38, 36,
        49, 47, 37, 36,]

index_0 = [35, 143]
index_h = [37, 139]

index = [i for i in range(len(data))]
normal_index = index
for i in index_0:
    normal_index.remove(i)

no_0_data = data
for i in index_0:
    no_0_data.remove(0)

ticklabels = [0, 50, 100, 200, 250]

to_plot = [{
    "type": PlotterType.MULTISCATTER,
    "data": [[normal_index, no_0_data], [index_0, [0,0]]],
    "marker": [".", "."],
    "marker_size": [300, 500],
    "marker_color": ["#5dade2", "#c0392b"],
    "y_label": "Occurences",
    "x_label": "$C_{0}^{*}$",
    "y_label_fontsize": 40,
    "x_label_fontsize": 40,
    "y_ticklabels_fontsize": 30,
    "x_ticklabels": ["{}".format(v) for v in ticklabels] + ["0x{:02x}".format(i) for i in index_0],
    "x_ticklabels_position": ticklabels + index_0,
    "x_ticklabels_fontsize": 30,
    "legend": ["Occuring values", "Forbidden values"]
}]

pl = Plotter(to_plot, latex=True)
pl.show()
