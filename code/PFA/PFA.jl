include("AES.jl")


function load_texts(filepath::String)
    f_texts = open(filepath, "r")
    texts=map(x->hex2bytes(strip(x)),readlines(f_texts))
    close(f_texts)
  
    n=size(texts,1)
    p=size(texts[1],1)
    mt=zeros(n,p)
    for i in 1:n
        mt[i,:]=texts[i]
    end
    round.(UInt8,mt)
end

function create_distribution(input::Array{UInt8, 1})
    vec = zeros(256)
    for i in input
        vec[i+1] += 1
    end
    vec
end

function pfa(filepath::String)
    ciphertexts = load_texts(filepath)

    #count how many faults
    faults = length(findall(x -> x==0.0, create_distribution(ciphertexts[:,1])))

    results = zeros(UInt8, 16, faults)

    for i in 1:16
        v = create_distribution(ciphertexts[:,i])
        z = map(x -> x-1, findall(x -> x==0.0, v))
        results[i,:] = z
    end

    results
end


function verify_key(key, pt, ct)
    ct_verif = AES(pt, key)
    return ct_verif == ct
end

function gen_key(pfa_res, comb)
    bitvec = digits(comb, base=2, pad=16)
    key = zeros(UInt8, 16)
    for i in 1:16
        key[i] = pfa_res[i,bitvec[i] + 1]
    end
    key
end

function brute_force(pfa_res, pt, ct)
    for y in 0:255
        print(".")
        for comb in 0:65535
            new_key = revertK10(gen_key(xor.(pfa_res, y), comb))
            if verify_key(new_key, pt, ct)
                return new_key
            end
        end
    end

    println("Error: key not found")
end